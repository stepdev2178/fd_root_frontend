import { resolve } from 'path';
import { defineConfig, loadEnv } from 'vite';
import vue2 from '@vitejs/plugin-vue2';
import inject from "@rollup/plugin-inject";
import commonjs from '@rollup/plugin-commonjs';

const handleAssetFileName = (assetInfo) => {
    const info = assetInfo.name.split('.');
    const extType = info[info.length - 1];

    // Карта розширень файлів для відповідних директорій
    const dirMap = {
        // Стилі
        css: 'css',

        // Шрифти
        woff: 'fonts',
        woff2: 'fonts',
        ttf: 'fonts',
        eot: 'fonts',

        // Зображення
        jpg: 'images',
        jpeg: 'images',
        png: 'images',
        svg: 'images',
        gif: 'images',
        webp: 'images',
        avif: 'images',

        // Медіа
        mp4: 'media',
        webm: 'media',
        mp3: 'media',
        wav: 'media',

        // Документи
        pdf: 'docs',
        doc: 'docs',
        docx: 'docs',
        xls: 'docs',
        xlsx: 'docs',
    }

    const dir = dirMap[extType] || 'assets';
    return `${dir}/[name].[hash][extname]`;
}

export default ({ mode }) => {
    const { VITE_PORT, VITE_BASE_URL } = loadEnv(mode, process.cwd());

    const isDevelopment = mode === 'development';

    return defineConfig({
        base: VITE_BASE_URL,
        publicDir: 'static', // вказуємо директорію зі статичними файлами
        define: {
            global: 'globalThis'
        },
        plugins: [
            commonjs(),
            inject({
                include: ['**/*.js'],
                exclude: ['**/*.vue', '**/*.css', '**/*.scss', '**/*.sass',],
            }),
            vue2(),
        ],
        resolve: {
            extensions: ['.vue', '.js', '.json'],
            alias: {
                'vue': 'vue/dist/vue.esm.js',
                '@': resolve('src'),
                '@components': resolve(__dirname, 'src/components'),
            },
        },
        css: {
            preprocessorOptions: {
                scss: {
                }
            },
        },
        server: {
            https: false,
            port: VITE_PORT,
            host: '0.0.0.0',
            open: true,
            cors: true,
            proxy: {},
        },
        build: {
            rollupOptions: {
                input: {
                    main: resolve(__dirname, 'index.html'),
                },
                output: {
                    entryFileNames: 'js/[name].[hash].js',
                    chunkFileNames: 'js/chunks/[name].[hash].js',
                    // Всі інші assets
                    assetFileNames: handleAssetFileName,
                }
            },
            minify: !isDevelopment ? 'terser' : false,
            terserOptions: !isDevelopment ? {
                compress: {
                    drop_console: false,
                    drop_debugger: true
                }
            } : undefined,
            sourcemap: isDevelopment,
            chunkSizeWarningLimit: 2000,
            reportCompressedSize: true,
        },
    });
};
