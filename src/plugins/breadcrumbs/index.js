// Check do we need this as new plugin for breadcrumbs

const defaultOptions = {
    registerComponent: true,
    template: `
    <nav class="breadcrumb" v-if="crumbs.length">
      <router-link 
        class="breadcrumb-item" 
        v-for="(crumb, key) in crumbs" 
        :to="linkProp(crumb)" 
        :key="key"
      >
        {{ crumbText(crumb) }}
      </router-link>
    </nav>
  `
};

// Main plugin object
const VueBreadcrumbs = {
    install(Vue, userOptions = {}) {
        const options = { ...defaultOptions, ...userOptions };

        // Register global breadcrumbs component if enabled
        if (options.registerComponent) {
            Vue.component('breadcrumbs', {
                template: options.template,
                computed: {
                    crumbs() {
                        const breadcrumbs = [];
                        let currentRoute = this.$route;

                        while (currentRoute) {
                            // Handle both Vue 1.x and 2.x route formats
                            const breadcrumb = currentRoute.meta?.breadcrumb || currentRoute.breadcrumb;

                            if (breadcrumb) {
                                breadcrumbs.unshift({
                                    name: breadcrumb,
                                    path: currentRoute.path,
                                    meta: currentRoute.meta || {}
                                });
                            }

                            currentRoute = currentRoute.matched?.[0]?.parent || currentRoute.parent;
                        }

                        return breadcrumbs;
                    }
                },
                methods: {
                    linkProp(crumb) {
                        return { path: crumb.path };
                    },
                    crumbText(crumb) {
                        return crumb.name;
                    }
                }
            });
        }

        // Add crumbText filter for customizing breadcrumb display
        Vue.filter('crumbText', (crumb) => {
            return crumb.name;
        });
    }
};

// Export plugin object
export default VueBreadcrumbs;
