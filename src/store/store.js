import Vue from 'vue';
import Vuex from 'vuex'
import toastr from 'toastr';
import utils from './modules/utils'
import video from './modules/video'
import photosList from './modules/photosList'

Vue.use( Vuex );

export default new Vuex.Store({
  modules: {
      utils,
      video,
      photosList
    },
  state: {
    modals: {
      profileModal: {
        open: false,
        loaded: false,
        user: {
          birthday: null,
          profile_additional_info: {},
          profile: {
            name: '',
            age: '',
            family_status: '',
            children: '',
            address_city: '',
            address_country: '',
            religion: '',
            profession: '',
            interests: '',
            about_me: '',
            about_man: '',
            about_woman_text: '',
            target_friendship: false,
            target_communication: false,
            target_journey: false,
            target_flirtation: false,
            target_meeting: false,
            target_family: false,
            target_children: false,
          }
        }
      }
    },
    app: {
      config: {},
      user: {
        auth: false,
        accessLevel: 'root',
        identity: {
          avatar: {
            small: '#',
            name: ''
          }
        },
        rules: []
      },
      rules: {
        finance: {
          finance_access_refill: 'просмотр "пополнения"',
          finance_access_bonus: 'просмотр "бонуса"',
          finance_access_create_bonus: 'создание "бонуса"',
          finance_access_edit_bonus: 'редактирование "бонуса"',
          finance_access_penalty: 'просмотр "штрафов"',
          finance_access_create_penalty: 'создание "штрафа"',
          finance_access_edit_penalty: 'редактирование "штрафа"',
          finance_access_transaction: 'просмотр "транзакции"',
          finance_access_agency_accruals: 'просмотр "начисления парнеров"',
          finance_access_agency_payments: 'просмотр "выплаты партнеров"',
          finance_access_edit_agency_payments: 'редактирование "выплаты партнеров"'
        },
        partner: {
          access_agency: 'просмотр "партнеры"',
          edit_agency: 'редактирование "партнера"',
          create_agency: 'создание "партнера"',
        },
        women: {
          access_woman: 'просмотр "клиентки"',
          access_woman_email: 'доступ к email "клиентки"',
          edit_woman: 'редактирование "клиентки"',
          create_woman: 'создание "клиентки"',
        },
        man: {
          access_man: 'просмотр "клиенты"',
          access_man_email: 'доступ к email "клиента"',
          create_man: 'редактирование "клиента"',
          edit_man: 'создание "клиента"',
        },
        services: {
          service_access_message: 'просмотр "письма"',
          service_edit_message: 'редактирование "письма"',
          service_create_message: 'создание "письма"',
          service_access_distribution: 'просмотр "рассылки"',
          service_create_distribution: 'создание "рассылки"',
          service_edit_distribution: 'редактирование "рассылки"',
          service_access_chat_log: 'просмотр "логи чатов"',
          service_access_gift: 'просмотр "подарки"',
          service_edit_gift: 'редактирование "подарка"',
          service_access_realinfo_request: 'просмотр "запрос контакт инфо"',
          service_edit_realinfo_request: 'редактирование "запрос контакт инфо"'
        },
        moderation: {
          access_moderation_woman_change_profile: 'просмотр "клиентки (изменение профайла)"',
          moderation_woman_change_profile: 'модерация "клиентки (изменение профайла)"',
          access_moderation_woman_new_profile: 'просмотр "клиентки (новые)"',
          moderation_woman_new_profile: 'модерация "клиентки (новые)"',
          access_moderation_woman_photo: 'просмотр "фото клиенток"',
          moderation_woman_photo: 'модерация "фото клиенток"',
          access_moderation_woman_distribution: 'просмотр "рассылки клиенток"',
          moderation_woman_distribution: 'модерация "рассылки клиенток"',
          access_moderation_woman_avatar: 'просмотр "аватары клиентов"',
          moderation_woman_avatar: 'модерация "аватары клиентов"',
          access_moderation_photo_delete_request: 'просмотр "запросы на удаление фото клиенток"',
          moderation_woman_photo_delete_request: 'модерация "запросы на удаление фото клиенток"',
          access_moderation_woman_gift_sent: 'просмотр "запросы подарков"',
          moderation_woman_gift_sent: 'модерация "запросы подарков"'
        },
        feedback: {
          access_feedback_woman: 'просмотр "обр.связь клиентки"',
          feedback_woman_response: 'редактирование "обр.связь клиентки"',
          feedback_man: 'просмотр "обр.связь клиенткы"',
          feedback_man_response: 'редактирование "обр.связь клиента"',
          feedback_agency: 'просмотр "обр.связь партнеры"',
          feedback_agency_response: 'редактирование "обр.связь партнеры"',
          feedback_guest: 'просмотр "обр.связь гости"',
          feedback_guest_response: 'редактирование "обр.связь гости"'
        },
        settings: {
          setting_access_credit_pack: 'просмотр "пакеты кредитов"',
          setting_create_credit_pack: 'создание "пакеты кредитов"',
          setting_edit_credit_pack: 'редактирование "пакеты кредитов"',
          setting_access_agency_type: 'просмотр "типы партнеров"',
          setting_create_agency_type: 'создание "типы партнеров"',
          setting_edit_agency_type: 'редактирование "типы партнеров"',
          setting_access_price_list: 'просмотр "стоимость услуг"',
          setting_edit_price_list: 'редактирование "стоимость услуг"',
          setting_access_gift: 'просмотр "подарки сервиса"',
          setting_create_gift: 'создание "подарка сервиса"',
          setting_edit_gift: 'редактирование "подарка сервиса"',
          setting_access_penalty: 'просмотр "настройки штрафов"',
          setting_edit_penalty: 'редактирование "настройки штрафов"',
          setting_access_bonus_category: 'просмотр "категории бонусов"',
          setting_create_bonus_category: 'создание "категории бонусов"',
          setting_edit_bonus_category: 'редактирование "категории бонусов"',
          setting_access_site: 'просмотр "настройки сервиса"',
          setting_edit_site: 'редактирование "настройки сервиса"'
        },
        notifications: {
          notification_access_woman: 'просмотр "уведомления клиенткам"',
          notification_create_woman: 'создание "уведомления клиенткам"',
          notification_edit_woman: 'редактирование "уведомления клиенткам"',
          notification_access_man: 'просмотр "уведомления клиентам"',
          notification_edit_man: 'редактирование "уведомления клиентам"',
          notification_create_man: 'создание "уведомления клиентам"',
          notification_access_agency: 'просмотр "уведомления партнерам"',
          notification_create_agency: 'создание "уведомления партнерам"',
          notification_edit_agency: 'редактирование "уведомления партнерам"'
        }
      },
      chatWomen: [],
      currentChatUser: null,
      currentChatWoman: null,
      chatInvitesArray: [],
      chatSoundChat: true,
      chatSoundActiveChat: true,
      chatInvites: true,
      chatActive: false,
      chatActiveClock: false,
      currentTypedMessage: '',
    },
    stats: {
      agency: {},
      feedback: {},
      finance: {},
      man: {},
      moderation: {},
      services: {},
      visitors: {},
      woman: {}
    },
    generalSettings: {},
    countersModeration: {},
    countersFeedback: {},
	  countersWomen: {},
	  countersMan: {}
  },
  actions: {
    getGeneralSettings(context, payload) {
      Vue.http.post( 'v2/site/settings/get/general', {
        access_token: Vue.cookie.get('access_token'),
      } )
          .then( response => {
            response.json()
                .then( response => {
                  context.commit( 'SET_GENERAL_SETTINGS', response.result );
                }, error => {
                  console.error( error )
                } )
          }, error => {
            console.log( error )
          } )
    },


    getCountersModeration(context, payload) {
      Vue.http.post( 'v2/moderation/counter', {
        access_token: Vue.cookie.get('access_token'),
      } )
        .then( response => {
          response.json()
            .then( response => {
              context.commit( 'SET_COUNTERS_MODERATION', response.result );
            }, error => {
              console.error( error )
            } )
        }, error => {
          console.log( error )
        } )
    },
    getCountersFeedback(context, payload) {
      Vue.http.post( 'v2/feedback/counter', {
        access_token: Vue.cookie.get('access_token'),
      } )
        .then( response => {
          response.json()
            .then( response => {
              context.commit( 'SET_COUNTERS_FEEDBACK', response.result );
            }, error => {
              console.error( error )
            } )
        }, error => {
          console.log( error )
        } )
    },
	getCountersUsers( context, payload ) {
		  Vue.http.post( 'v2/female/counter', {
			  access_token: Vue.cookie.get( 'access_token' ),
		  } )
			  .then( response => {
				  response.json()
					  .then( response => {
						  context.commit( 'SET_COUNTERS_WOMEN', response.result );
					  }, error => {
						  console.error( error )
					  } )
			  }, error => {
				  console.log( error )
			  } )

		  Vue.http.post( 'v2/male/counter', {
			  access_token: Vue.cookie.get( 'access_token' ),
		  } )
			  .then( response => {
				  response.json()
					  .then( response => {
						  context.commit( 'SET_COUNTERS_MAN', response.result );
					  }, error => {
						  console.error( error )
					  } )
			  }, error => {
				  console.log( error )
			  } )
	  },
    getStats(context, payload) {
      Vue.http.post('v2/dashboard/get/statistic', {
          access_token: Vue.cookie.get('access_token'),
          time_from: payload.from,
          time_to: payload.to,
        })
        .then(response => {
          response.json()
            .then(response => {
              context.commit('GET_STATS', response.result);
            }, error => {
              console.error(error)
            })
        }, error => {
          console.log(error)
        })
    },
    setAuthStatus({commit}, payload) {
      commit('SET_AUTH', payload);
    },
    setAccessToken({commit}, payload) {
      commit('SET_TOKEN', payload)
    },
    setIdentity({commit}, payload) {
      commit('SET_IDENTITY', payload);
    },
    setAccessLevel({commit}, payload) {
      commit('SET_ACCESS_LEVEL', payload);
    },
    userRules({commit}, payload) {
      commit('SET_RULES', payload)
    },
    socket_online({commit}, payload) {
      commit('CHAT_REMOVE_WOMAN_FROM_LIST', {
        id: parseInt(payload.id)
      })
    },
    socket_photoBuy({commit, state}, payload) {
      commit('MARK_PHOTO', payload);
    },
    socket_message({commit}, payload) {
      if (payload.photoId || payload.photoSell) {
        commit('CHAT_PUSH_MESSAGE_TO_DIALOG', {
          type: 'photo',
          user_id: payload.sender,
          receiver: payload.receiver,
          text: '_',
          photo: +payload.photoId,
          isInvite: false,
          start: false,
          invite: false,
          id: Math.floor(Date.now()),
        })
      } else if ('messageObject' in payload) {
        commit('CHAT_PUSH_MESSAGE_TO_DIALOG', {
          user_id: payload.sender,
          target_user_id: payload.receiver,
          text: payload.messageObject.text,
          type: payload.messageObject.type,
          isInvite: payload.messageObject.freeInvite,
          start: payload.messageObject.chatStartMessage,
          invite: payload.messageObject.freeInvite,
          id: payload.messageObject._id,
        })
      } else {
        commit('CHAT_PUSH_MESSAGE_TO_DIALOG', {
          user_id: payload.sender,
          target_user_id: payload.receiver,
          text: payload.text,
          type: payload.type,
          isInvite: payload.freeInvite,
          start: payload.chatStartMessage,
          invite: payload.freeInvite,
          id: payload._id,
        })
      }
    },
    socket_notification({state, commit}, payload) {
      if (payload.type === 'invite') {
        if (state.app.chatSoundChat) {
          window.fInv.play();
        }
        if (state.app.currentChatWoman !== null && state.app.currentChatWoman.id === payload.params.receiver && state.app.chatInvites) {
          toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "showDuration": "60000",
            "hideDuration": "60000",
            "timeOut": "60000",
            "extendedTimeOut": "60000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          };
          toastr.options.onclick = () => {
            commit('CHAT_SET_USER_ACTIVE', payload.params.senderId)
          };
          toastr["info"](`${payload.params.senderName} <b>(${payload.params.sender})</b> <br> ${payload.params.text}`);
        }

        state.app.chatWomen.forEach((woman) => {
          if (woman.id === payload.params.receiver) {
            let iterationCounter = 0;
            woman.onlineList = Object.assign([], woman.onlineList);
            woman.onlineList.forEach(user => {
              if (user.id === payload.params.senderId) {
                console.log('set invite for woman list', woman.id);
                state.app.chatInvitesArray.push({
                  user: payload.params.senderId,
                  target: woman.id,
                })
              } else {
                iterationCounter++;
              }
            });
            iterationCounter = 0;
            woman.contactsList.forEach((user) => {
              if (user.id === payload.params.senderId) {
                user.invite = true;
              } else {
                iterationCounter++;
              }
            });
            iterationCounter = 0;
            woman.favoritesList.forEach((user) => {
              if (user.id === payload.params.senderId) {
                user.invite = true;
              } else {
                iterationCounter++;
              }
            });
            iterationCounter = 0;
            woman.bookmarksList.forEach((user) => {
              if (user.id === payload.params.senderId) {
                user.invite = true;
              } else {
                iterationCounter++;
              }
            });
          }
        });
      }
    },
    socket_chatEvent({state, commit}, payload) {
      let targetUser = null;
      let sender = null;

      if (state.app.currentChatWoman === null || !'users' in payload || payload.users.length < 2) return false;
      if (payload.users[0].user === state.app.currentChatWoman.id) {
        try {
          targetUser = parseInt(payload.users[1].user);
          sender = parseInt(payload.users[0].user);
        } catch (ex) {
        }
      } else {
        try {
          targetUser = parseInt(payload.users[0].user);
          sender = parseInt(payload.users[1].user);
        } catch (ex) {
        }
      }
      if (payload.event === "setActiveChat") {
        state.app.currentChatWoman.dialogsList.list.forEach(dialog => {
          if (dialog.user.id === targetUser) {
            if (dialog.chatActiveClock || dialog.chatActive) return false;

            dialog.chatActive = true;
            dialog.chatActiveClock = true;
            dialog.messages.push({
              text: '_',
              created: Math.floor(Date.now() / 1000 - 100),
              id: Math.floor(Date.now()),
              type: 'normalMessage',
              view: 'info',
              action: 'start',
              reade: true,
              sender: sender,
              receiver: targetUser,
              isInvite: false
            });
          }
        });
        if (state.app.currentChatWoman.dialogsList.current.user.id === targetUser) {
          state.app.chatActiveClock = true;
          if (state.app.currentChatWoman.dialogsList.current.chatActive) return false;
          state.app.currentChatWoman.dialogsList.current.chatActive = true;
          state.app.currentChatWoman.dialogsList.current.messages.push({
            text: '_',
            created: Math.floor(Date.now() / 1000 - 100),
            id: Math.floor(Date.now()),
            type: 'normalMessage',
            view: 'info',
            action: 'start',
            sender: sender,
            reade: true,
            receiver: targetUser,
            isInvite: false
          });
        }
      }
      if (payload.event === "setCloseChat") {
        console.log('set pause chat');
        state.app.currentChatWoman.dialogsList.list.forEach(dialog => {
          if (dialog.user.id === targetUser) {
            if (dialog.chatActive === false) return false;
            console.log('stop in woman list');
            dialog.chatActive = false;
            dialog.chatActiveClock = false;
            dialog.messages.push({
              text: '_',
              created: Math.floor(Date.now() / 1000 - 100),
              id: Math.floor(Date.now()),
              type: 'normalMessage',
              view: 'info',
              action: 'end',
              sender: sender,
              reade: true,
              receiver: targetUser,
              isInvite: false
            });
          }
        });
        if (state.app.currentChatWoman.dialogsList.current !== null && state.app.currentChatWoman.dialogsList.current.user.id === targetUser) {
          state.app.chatActiveClock = false;
          if (state.app.currentChatWoman.dialogsList.current.chatActive === false) return false;
          state.app.currentChatWoman.dialogsList.current.chatActive = false;
          state.app.currentChatWoman.dialogsList.current.messages.push({
            text: '_',
            created: Math.floor(Date.now() / 1000 - 100),
            id: Math.floor(Date.now()),
            type: 'normalMessage',
            view: 'info',
            reade: true,
            action: 'end',
            sender: sender,
            receiver: targetUser,
            isInvite: false
          });
        }
      }
    },
  },
  mutations: {
      SET_GENERAL_SETTINGS( state, settings ) {
        state.generalSettings = settings;
      },
	  SET_COUNTERS_WOMEN( state, counters ) {
		  state.countersWomen = counters
	  },
	  SET_COUNTERS_MAN( state, counters ) {
		  state.countersMan = counters
	  },
    SET_COUNTERS_MODERATION( state, counters ) {
      state.countersModeration = counters
    },
    SET_COUNTERS_FEEDBACK( state, counters ) {
      state.countersFeedback = counters
    },
    SET_CONFIG(state, payload) {
      state.app.config = payload;
    },
    GET_STATS(state, stats) {
      state.stats = stats
    },
    SET_RULES(state, rules) {
      state.app.user.rules = rules;
    },
    SET_AUTH(state, auth) {
      state.app.user.auth = auth;
    },
    SET_TOKEN(state, token) {
      state.app.user.token = token;
    },
    SET_IDENTITY(state, identity) {
      state.app.user.identity = identity;
    },
    SET_ACCESS_LEVEL(state, level) {
      state.app.user.accessLevel = level;
    },
    CHAT_NOTIFICATIONS_SET(state, payload) {
      state.app.chatInvites = payload;
    },
    CHAT_DROP_WOMAN_SESSION(state, payload) {
      state.app.chatWomen = [];
      state.app.currentChatWoman = null;
      state.app.currentChatUser = null;
    },
    MARK_PHOTO(state, payload) {
      state.app.chatWomen.forEach(woman => {
        if (woman.id === parseInt(payload.target_user_id)) {
          woman.dialogsList.list.forEach(dialog => {
            dialog.messages.forEach(message => {
              if (parseInt(message.photo) === parseInt(payload.photo_id)) {
                message.bought = true;
              }
            })
          })
        }
      })
    },
    CHAT_STOP_ALL_CHATS(state, payload) {
      state.app.chatWomen.forEach(woman => {
        woman.dialogsList.list.forEach(dialog => {
          dialog.chatActive = false;
        });
        if (woman.dialogsList.current !== null) {
          woman.dialogsList.current.chatActive = false;
        }
      });

      if (state.app.currentChatWoman !== null) {
        state.app.currentChatWoman.dialogsList.list.forEach(dialog => {
          dialog.chatActive = false;
        });
        if (state.app.currentChatWoman.dialogsList.current !== null) {
          state.app.currentChatWoman.dialogsList.current.chatActive = false;
        }
      }

      state.app.chatActiveClock = false;
    },
    CHAT_SET_USER_ACTIVE(state, userId) {
      if (state.app.currentChatWoman === null) return false;
      let targetUser = null;
      state.app.currentChatWoman.onlineList.forEach((onlineItem) => {
        if ('id' in onlineItem && onlineItem.id === parseInt(userId)) {
          targetUser = userId;

          let dialogExists = false;
          state.app.currentChatWoman.dialogsList.list.forEach(dialog => {
            if (dialog.user.id === userId) {
              dialogExists = true;
              state.app.currentChatWoman.dialogsList.current = dialog;
            }
          });

          if (!dialogExists) {
            state.app.currentChatWoman.dialogsList.list.push({
              user: onlineItem,
              messages: [],
            });
            state.app.currentChatWoman.dialogsList.list.forEach(d => {
              if (d.user.id === onlineItem.id) {
                state.app.currentChatWoman.dialogsList.current = d;
              }
            })
          }

          state.app.currentChatWoman.currentChatUser = onlineItem;

          state.app.currentChatWoman.onlineList.forEach(item => {
            if (item.id === onlineItem.id) {
              let counter = 0;
              state.app.chatInvitesArray.forEach(item => {
                if(item.user === onlineItem.id) {
                  state.app.chatInvitesArray.splice(counter, 1);
                }else {
                  counter++;
                }
              })
            }
          });

          state.app.currentChatWoman.bookmarksList.forEach(item => {
            if (item.id === onlineItem.id) {
              item.invite = false;
            }
          });

          state.app.currentChatWoman.favoritesList.forEach(item => {
            if (item.id === onlineItem.id) {
              item.invite = false;
            }
          });

          state.app.currentChatWoman.contactsList.forEach(item => {
            if (item.id === onlineItem.id) {
              item.invite = false;
            }
          });

          state.app.currentChatUser = onlineItem;
          state.app.chatActiveClock = state.app.currentChatWoman.dialogsList.current.chatActive;
        }
      });
      if (targetUser === null) {
        Vue.http.post(state.app.config.api.frontend + '/v2/profile/load', {
          access_token: state.app.currentChatWoman.token.site,
          target_user_id: userId
        }).then(data => {
          if (data.body.result) {
            const user = data.body.result;
            state.app.currentChatUser = {
              id: parseInt(user.user_id),
              name: user.name,
              online: user.online,
              isNew: user.new,
              avatar: user.avatar,
            };

            let woman;
            state.app.chatWomen.forEach(woman => {
              if (woman.id === state.app.currentChatWoman.id) {
                woman.currentChatUser = state.app.currentChatUser;
                woman.dialogsList.current = {
                  user: null,
                  messages: [],
                };
                woman.dialogsList.current.user = state.app.currentChatUser;
                woman.dialogsList.current.messages = [];
                woman.dialogsList.list.push({
                  user: state.app.currentChatUser,
                  messages: []
                });
                state.app.currentChatWoman = woman;
              }
            });
          }
        }).catch(ex => {
          console.log(ex)
        });
      }
    },
    CHAT_SET_SOUND_CHAT(state, payload) {
      state.app.chatSoundChat = payload;
    },
    CHAT_SET_SOUND_ACTIVE_CHAT(state, payload) {
      state.app.chatSoundActiveChat = payload;
    },
    CHAT_REMOVE_WOMAN_FROM_LIST(state, payload) {
      let counter = 0;
      state.app.chatWomen.forEach(woman => {
        if (parseInt(woman.id) === payload.id) {
          state.app.chatWomen.splice(counter, 1);
        } else {
          counter++;
        }
      });

      if (state.app.chatWomen.length === 0) {
        state.app.currentChatWoman = null;
        state.app.currentChatUser = null;
        state.app.chatActiveClock = false;
        state.app.chatActive = false;
      } else if (state.app.currentChatWoman !== null && state.app.currentChatWoman.id === payload.id) {
        state.app.currentChatWoman = state.app.chatWomen[0];
        state.app.currentChatUser = state.app.chatWomen[0].dialogsList.current.user;
      }
    },
    CHAT_ADD_WOMAN_TO_LIST(state, payload) {
      state.app.chatWomen.unshift(payload);
    },
    CHAT_SET_CURRENT_WOMAN(state, payload) {
      state.app.chatWomen.forEach(woman => {
        if (parseInt(woman.id) === parseInt(payload)) {
          state.app.currentChatWoman = woman;
          state.app.currentChatUser = state.app.currentChatWoman.currentChatUser || null;
          if (state.app.currentChatWoman.dialogsList.current !== null) {
            state.app.chatActiveClock = state.app.currentChatWoman.dialogsList.current.chatActive;
            state.app.currentChatWoman.dialogsList.current.messages.forEach(message => {
              message.reade = true;
            });
          }
        }
      })
    },
    CHAT_WOMAN_INIT_LIST(state, payload) {
      state.app.chatWomen = payload;
    },
    CHAT_SET_ONLINE_LIST(state, payload) {
      state.app.chatWomen.forEach(woman => {
        if (woman.id === parseInt(payload.woman_id)) {
          woman.onlineList = Object.assign([], payload.list);
        }
      });
    },
    CHAT_SET_BOOKMARK_LIST(state, payload) {
      state.app.chatWomen.forEach(woman => {
        if (woman.id === parseInt(payload.woman_id)) {
          woman.bookmarksList = payload.list;
        }
      });
      if (state.app.currentChatWoman !== null && state.app.currentChatWoman.id === payload.woman_id) {
        state.app.currentChatWoman.bookmarksList = payload.list;
      }
    },
    CHAT_SET_FAVORITE_LIST(state, payload) {
      state.app.chatWomen.forEach(woman => {
        if (woman.id === parseInt(payload.woman_id)) {
          woman.favoritesList = payload.list;
        }
      });
      if (state.app.currentChatWoman !== null && state.app.currentChatWoman.id === payload.woman_id) {
        state.app.currentChatWoman.favoritesList = payload.list;
      }
    },
    CHAT_SET_CONTACT_LIST(state, payload) {
      state.app.chatWomen.forEach(woman => {
        if (woman.id === parseInt(payload.woman_id)) {
          woman.contactsList = payload.list;
        }
      });
      if (state.app.currentChatWoman !== null && state.app.currentChatWoman.id === payload.woman_id) {
        state.app.currentChatWoman.contactsList = payload.list;
      }
    },
    CHAT_SET_ONLINE_LIST_UPDATED(state, payload) {
      state.app.chatWomen.forEach(woman => {
        if (woman.id === parseInt(payload.woman_id)) {
          woman.listUpdated = Math.floor(Date.now() / 1000);
        }
      })
    },
    CHAT_TOGGLE_CHAT_STATUS(state, payload) {
      state.app.chatActive = payload;
    },
    CHAT_SET_CURRENT_TYPED_MESSAGE(state, payload) {
      if (state.app.currentChatWoman === null) return false;
      state.app.currentChatWoman.currentTypedMessage = payload;
    },
    CHAT_STOP_CHAT_BY_ID(state, payload) {
      state.app.currentChatWoman.dialogsList.list.forEach(dialog => {
        if (dialog.user.id === payload) {
          dialog.chatActive = false;
          if (state.app.currentChatWoman.dialogsList.current.user.id === payload) {
            state.app.chatActiveClock = false;
            state.app.currentChatWoman.dialogsList.current.chatActive = false;
          }
        }
      })
    },
    CHAT_READ_MESSAGE_IN_CURRENT_DIALOG(state, payload) {
      state.app.currentChatWoman.dialogsList.current.messages.forEach(message => {
        if (message.sender === payload.user_id || message.receiver === payload.user_id) {
          state.app.currentChatWoman.dialogsList.current.unreadedMessages = 0;
          message.reade = true;
        }
      });
      state.app.currentChatWoman.dialogsList.current.unreadedMessages = 0;
      state.app.currentChatWoman.dialogsList.list.forEach(dialog => {
        if (dialog.user.id === payload.user_id) {
          dialog.unreadedMessages = 0;
          dialog.messages.forEach(message => {
            message.reade = true;
          })
        }
      })
    },
    CHAT_PUSH_MESSAGE_TO_DIALOG(state, payload) {

      const messageObject = {
        text: payload.text,
        created: Math.floor(Date.now() / 1000 - 100),
        id: payload.id,
        type: payload.type,
        view: 'message',
        bought: false,
        photo: payload.photo || 0,
        sender: payload.user_id,
        receiver: payload.target_user_id,
        isInvite: payload.invite
      };

      if (payload.photo) {
        messageObject.view = 'photo';
      }

      const playSound = () => {
        if (state.app.chatSoundChat) {
          window.newMessageAudio.play();
        }
      };

      state.app.chatWomen.forEach(woman => {
        if (woman.id === payload.target_user_id || woman.id === payload.user_id) {
          messageObject.reade = false;
          if (state.app.currentChatWoman !== null
            && woman.id === state.app.currentChatWoman.id
            && state.app.currentChatWoman.dialogsList.current !== null
            && (state.app.currentChatWoman.dialogsList.current.user.id === payload.user_id
              || state.app.currentChatWoman.dialogsList.current.user.id === payload.target_user_id)) {
            messageObject.reade = true;
          }
          let dialogExists = false;
          woman.dialogsList.list.forEach(dialog => {
            if (dialog.user.id === payload.user_id || dialog.user.id === payload.target_user_id) {
              dialog.lastManActive = Math.floor(Date.now() / 1000);
              dialogExists = true;
              if (messageObject.reade === false) {
                dialog.unreadedMessages += 1;
              }
              dialog.messages.push(messageObject);
              playSound();
            }
          });
          if (dialogExists === false) {
            let newDialogUser = null;
            woman.onlineList.forEach(user => {
              if (user.id === payload.user_id || user.id === payload.target_user_id) {
                newDialogUser = user;
              }
            });
            if (newDialogUser === null) {
              return false;
            }
            const dialogObject = {
              user: newDialogUser,
              lastManActive: Math.floor(Date.now() / 1000),
              chatActive: false,
              unreadedMessages: 0,
              messages: []
            };
            if (messageObject.reade === false) {
              dialogObject.unreadedMessages += 1;
            }
            dialogObject.messages.push(messageObject);

            woman.dialogsList.list.push(dialogObject);

            playSound();
          }
        }
      })
    },
    SET_CURRENT_TYPED_MESSAGE(state, payload) {
      state.app.currentTypedMessage = payload
    },
    CHAT_UPDATE_PROFILE_MODAL(state, payload) {
      state.modals.profileModal = payload;
    },
    CHAT_SET_CLOCK_ACTIVITY(state, payload) {
      state.app.chatActiveClock = payload;
    },
    CHAT_REMOVE_INVITE(state, payload) {
      return true;
    }
  },
  getters: {
      generalSettings( state ) {
        return state.generalSettings
      },
	  countersWomen( state ) {
		  return state.countersWomen
	  },
	  countersMan( state ) {
		  return state.countersMan
	  },
    moderationCounters(state) {
      return state.countersModeration
    },
    feedbackCounters(state) {
      return state.countersFeedback
    },
    chatInvitesArray(state) {
      if(state.app.chatInvitesArray === null) return [];
      return state.app.chatInvitesArray;
    },
    userWithInvites(state) {
      let idsArray = [];
      if(state.app.currentChatWoman === null) return [];
      state.app.chatWomen.forEach(woman => {
        if(woman.id ===  state.app.currentChatWoman.id) {
          woman.onlineList.forEach(man => {
            if (man.invite) {
              idsArray.push(man.id);
            }
          });
        }
      });
      return idsArray;
    },
    activeChatSound(state) {
      return state.app.chatSoundActiveChat;
    },
    chatSounds(state) {
      return state.app.chatSoundChat;
    },
    currentTypedMessage(state) {
      if (state.app.currentChatWoman === null) return '';
      return state.app.currentChatWoman.currentTypedMessage;
    },
    profileModal(state) {
      return state.modals.profileModal;
    },
    chatActiveClock(state) {
      return state.app.chatActiveClock;
    },
    dialog(state) {
      if (state.app.currentChatWoman === null) return {
        chatActive: false,
        messages: [],
      };
      return state.app.currentChatWoman.dialogsList.current;
    },
    womanChatToken(state) {
      if (state.app.currentChatWoman === null) return false;
      return state.app.currentChatWoman.token.chat;
    },
    womanSiteToken(state) {
      if (state.app.currentChatWoman === null) return false;
      return state.app.currentChatWoman.token.site;
    },
    chatStatus(state) {
      return state.app.chatActive;
    },
    womanToken: function (state) {
      if (state.app.currentChatWoman !== null) {
        return state.app.currentChatWoman.access_token;
      }
      return null;
    },
    currentChatUser(state) {
      return state.app.currentChatUser;
    },
    currentChatWoman(state) {
      return state.app.currentChatWoman;
    },
    chatWomenList(state) {
      return state.app.chatWomen;
    },
    media(state) {
      return state.app.media
    },
    rules(state) {
      return state.app.rules
    },
    auth(state) {
      return state.app.user.auth;
    },
    token(state) {
      if (state.app.user.token) {
        return state.app.user.token;
      } else {
        return false;
      }
    },
    user(state) {
      return state.app.user;
    },
    app(state) {
      return state.app;
    }
  }
});
