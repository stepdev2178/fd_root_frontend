import Vue from 'vue';
import { API } from '../../config/API';
const state = {
	videoMiniLoader: true,
    videosPublic: [],
    videosPrivate: [],
    videosPublicOnModeration: [],
    videosPrivateOnModeration: [],
    privateRejectedVideos: [],
    publicRejectedVideos: [],
    videosToDeleting: [],
	videoModal: {
		options: {},
		open: false
	},
	uploadVideoModal: {
		options: {},
		open: false
	},
    uploadPreviewVideo: {},
    videoIsChoose: false,
    moderateVideoModalState: {
        options: {},
        open: false,
        videoType: ''
    }
};

const getters = {
	// video
    videosPublic: state => { return state.videosPublic;},
    videosPrivate: state => { return state.videosPrivate;},
    videosPublicOnModeration: state => { return state.videosPublicOnModeration;},
    videosPrivateOnModeration: state => { return state.videosPrivateOnModeration;},
    privateRejectedVideos: state => { return state.privateRejectedVideos;},
    publicRejectedVideos: state => { return state.publicRejectedVideos;},
    videosToDeleting: state => { return state.videosToDeleting;},
    isVideoMiniLoaderAttached: state => {
        return state.videoMiniLoader;
    },
	ladyVideoApprove: state => {
        return state.ladyVideoApprove;
    },
	videoModalState( state ) {
		return state.videoModal;
	},
	uploadVideoModal( state ) {
		return state.uploadVideoModal;
	},
    uploadPreviewVideo( state ) {
        return state.uploadPreviewVideo;
    },
    moderateVideoModalState( state ) {
        return state.moderateVideoModalState;
    },
};

const mutations = {
	// video
    updateVideosPublic: (state, value) => {
        state.videosPublic = value;
    },
    updateVideosPrivate: (state, value) => {
        state.videosPrivate = value;
    },
    updateVideosPublicOnModeration: (state, value) => {
        state.videosPublicOnModeration = value;
    },
    updateVideosPrivateOnModeration: (state, value) => {
        state.videosPrivateOnModeration = value;
    },
    mixVideosPublicOnModeration: (state, value) => {
        state.videosPublicOnModeration = [...state.videosPublicOnModeration, value];
    },
    mixVideosPrivateOnModeration: (state, value) => {
        state.videosPrivateOnModeration = [...state.videosPrivateOnModeration, value];
    },
    updatePrivateRejectedVideos: (state, value) => {
        state.privateRejectedVideos = value;
    },
    updatePublicRejectedVideos: (state, value) => {
        state.publicRejectedVideos = value;
    },
    updateVideosToDeleting: (state, value) => {
        state.videosToDeleting = value;
    },
	updateVideoModalState( state, payload ) {
		Object.assign( state.videoModal, payload );
	},
    updateModerateVideoModalState( state, payload ) {
        state.moderateVideoModalState = Object.assign({}, state.moderateVideoModalState, payload );
    },
	updateUploadVideoModal( state, payload ) {
        state.uploadVideoModal = Object.assign( {}, state.uploadVideoModal, payload );
	},
    updateUploadPreviewVideo( state, payload ) {
        Object.assign( state.uploadPreviewVideo, payload );
    },
	attachVideoMiniLoader: (state) => {
        state.videoMiniLoader = true;
    },
    detachVideoMiniLoader: (state) => {
        state.videoMiniLoader = false;
    },
    updateLadyVideoApprove: (state, value) => {
        state.ladyVideoApprove = value;
    }
};

const actions = {
	// video  -------------------------------------------------
    getLadyVideos: ({ commit, dispatch }, data) => {
        return Vue.http.post(API.getLadyVideos.url, data)
            .then(res => res.json())
            .then(({ status, result }) => {
                if (status) {
                    commit('updateVideosPublic', result.public_video);
                    commit('updateVideosPrivate', result.private_video);
                    commit('updateVideosPublicOnModeration', result.public_on_moderate);
                    commit('updateVideosPrivateOnModeration', result.private_on_moderate);
                    commit('updatePrivateRejectedVideos', result.rejected_private_video);
                    commit('updatePublicRejectedVideos', result.rejected_public_video);
                    commit('updateVideosToDeleting', result.video_on_delete);
                } else {
                    throw new Error();
                }
            });
    },
    uploadLadyVideo: ({ commit, dispatch }, { type, data }) => {
        return Vue.http.post(API.uploadLadyVideo.url, data)
            .then(response => response.json())
            .then(({ status, result }) => {
                if (status) {
                    let action;
                    switch (type) {
                        case 'public':
                            action = 'mixVideosPublicOnModeration';
                            break;
                        case 'private':
                            action = 'mixVideosPrivateOnModeration';
                            break;
                    }
                    commit(action, result);
                    return Promise.resolve(result);
                } else {
                    throw new Error('Возникла ошибка при загрузке файла!');
                }
            });
    },
    uploadLadyVideoPreview: ({ commit, dispatch }, { type, data }) => {
        return Vue.http.post(API.uploadLadyVideoPreview.url, data)
            .then(response => response.json())
            .then(({ status, result }) => {
                if (status) {
                    let action;
                    switch (type) {
                        case 'public':
                            action = 'mixVideosPublicOnModeration';
                            break;
                        case 'private':
                            action = 'mixVideosPrivateOnModeration';
                            break;
                    }
                    commit(action, result);
                    return Promise.resolve(result);
                } else {
                    throw new Error('Возникла ошибка при загрузке файла!');
                }
            });
    },
    deleteLadyVideo: ({ dispatch }, data) => {
        return Vue.http.post(API.deleteLadyVideo.url, {
            video_id: data.video_id,
            user_id: data.user_id,
            access_token: Vue.cookie.get('access_token')
        }).then(response => response.json())
            .then(({ status }) => {
                if (status) {
                    return Promise.resolve('Видео было успешно удалено');
                } else {
                    throw new Error('Во время удаления видео произошла ошибка, попробуйте снова');
                }
            })
            .catch(() => {
                throw new Error('Во время удаления видео произошла ошибка, попробуйте снова');
            });
    },
    moderateLadyVideo: ({ dispatch }, data) => {
        return Vue.http.post('v2/moderation/moderate-new-video', data).then(response => response.json())
            .then(({ status, result }) => {
                if (status) {
                    let tc = data.video_moderation == 'accept' ? 'Видео успешно прошло модерацию' : 'Видео успешно отклонено';
                    return Promise.resolve(tc);
                } else {
                    throw new Error(
                        result && result.error
                            ? result.error
                            : 'Во время запроса произошла ошибка, попробуйте снова');
                }
            })
            .catch(err => {
                throw new Error(err)
            });
    },
    moderateLadyDeletedVideo: ({ dispatch }, data) => {
        return Vue.http.post('v2/moderation/moderate-video-delete-request', data)
            .then(response => response.json())
            .then(({ status }) => {
                if (status) {
                    var tc = data.video_moderation == 'accept' ? 'Видео успешно удалено' : 'Видео успешно отклонено';
                    return Promise.resolve(tc);
                } else {
                    throw new Error('Произошла ошибка, попробуйте снова');
                }
            })
            .catch(() => {
                throw new Error('Во время удаления видео произошла ошибка, попробуйте снова');
            });
    },
    changeLadyVideoAlbum: ({ dispatch }, data) => {
        return Vue.http.post('v2/female/move-video', data)
            .then(response => response.json())
            .then(({ status }) => {
                if (status) {
                    return Promise.resolve('Видео успешно перемещено!');
                } else {
                    throw new Error('Произошла ошибка, попробуйте снова!');
                }
            })
            .catch(() => {
                throw new Error('Произошла ошибка, попробуйте снова!');
            });
    },
    updateVideoSort: ({ dispatch }, data) => {
        return Vue.http.post('v2/user/set-video-order', data)
            .then(response => response.json())
    }
};

export default {
	state,
	getters,
	mutations,
	actions
}
