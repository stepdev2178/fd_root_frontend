import Vue from 'vue';
import { API } from '../../config/API';
let listLimit = 60;

const state = {
    photosLoaded: {
        photos: false, // для photos
        profilePhotos: false, // для profilePhotos
        memberPhotos: false, // для memberPhotos
    },

    // Общие лимиты и оффсеты для каждого объекта
    generalSettings: {
        photos: { limit: 30, offset: 0 },
        profilePhotos: { limit: 30, offset: 0 },
        memberPhotos: { limit: 30, offset: 0 },
    },

    // Основной объект photos
    photos: {
        avatars: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        avatar_on_moderate: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        rejected_avatar: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        public_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        public_on_moderate: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        rejected_public_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        private_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        private_on_moderate: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        trash_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        rejected_private_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        photo_on_delete: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
    },

    // Новый объект profilePhotos
    profilePhotos: {
        avatars: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        avatar_on_moderate: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        rejected_avatar: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        public_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        public_on_moderate: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        rejected_public_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        private_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        private_on_moderate: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        trash_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        rejected_private_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        photo_on_delete: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
    },

    // Новый объект memberPhotos
    memberPhotos: {
        avatars: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        public_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        private_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false },
        purchased_photo: { limit: listLimit, offset: 0, data: [], hasMore: true, loader: false }
    },

    photoTitles: {
        avatar_on_moderate: "Аватары на модерации",
        avatars: "Аватары",
        photo_on_delete: "Фото на удаление",
        private_on_moderate: "Приватные фото на модерации",
        private_photo: "Приватные фото",
        public_on_moderate: "Публичные фото на модерации",
        public_photo: "Публичные фото",
        rejected_avatar: "Отклоненные аватары",
        rejected_private_photo: "Отклоненные приватные фото",
        rejected_public_photo: "Отклоненные публичные фото",
        trash_photo: "Корзина",
        moderated_photo: "Модерированные фото",
        purchased_photo: 'Приобритенные фото'
    },
    photoClasses: {
        avatars: "text-blue",
        public_photo: "text-black",
        private_photo: "text-orange",
        avatar_on_moderate: "bg-blue text-white",
        private_on_moderate: "label-warning text-white",
        public_on_moderate: "label-success text-white",
        rejected_avatar: "text-red",
        rejected_private_photo: "text-red",
        rejected_public_photo: "text-red",
        photo_on_delete: "text-red",
        trash_photo: "text-red",
        moderated_photo: "text-green",
        purchased_photo: "text-orange"
    },
    photoUrlType: {
        avatars: "file_url_medium",
        public_photo: "file_url_medium",
        private_photo: "file_url_medium",
        avatar_on_moderate: "file_url_medium",
        private_on_moderate: "file_url_origin",
        public_on_moderate: "file_url_origin",
        rejected_avatar: "file_url_medium",
        rejected_private_photo: "file_url_origin",
        rejected_public_photo: "file_url_origin",
        photo_on_delete: "file_url_medium",
        trash_photo: "file_url_medium",
        moderated_photo: "file_url_medium",
        purchased_photo: "file_url_medium"
    },
};

const getters = {
    photos: state => state.photos,
    profilePhotos: state => state.profilePhotos,
    memberPhotos: state => state.memberPhotos,
    photosLoaded: state => key => state.photosLoaded[key],
    getPhotosTitle: state => type => {
        return state.photoTitles[type] || "Неизвестный альбом";
    },
    getClass: state => type => {
        return state.photoClasses[type] || "Неизвестный альбом";
    },
    photoUrlType: state => type => {
        return state.photoUrlType[type] || "file_url_origin";
    }
};

const mutations = {
    resetPhotosData(state, { objectName, type }) {
        const photos = state[objectName];
        if (!photos) return;

        if (type === 'all') {
            Object.keys(photos).forEach(key => {
                photos[key].data = [];
                photos[key].offset = 0;
                photos[key].hasMore = true;
            });
            state.generalSettings[objectName].offset = 0;
            state.photosLoaded[objectName] = false;
        } else if (photos[type]) {
            photos[type].data = [];
            photos[type].offset = 0;
            photos[type].hasMore = true;
        }
    },
    updatePhotos(state, { objectName, type, data, reset = false }) {
        const photos = state[objectName];
        if (!photos) return;

        if (type === 'all') {
            Object.keys(photos).forEach(key => {
                if (data[key]) {
                    Vue.set(
                        photos[key],
                        'data',
                        reset ? data[key] : [...photos[key].data, ...data[key]]
                    );
                    Vue.set(
                        photos[key],
                        'offset',
                        reset ? data[key].length : photos[key].offset + data[key].length
                    );
                    Vue.set(
                        photos[key],
                        'hasMore',
                        data[key].length === state.generalSettings[objectName].limit
                    );
                }
            });
        } else if (photos[type]) {
            Vue.set(
                photos[type],
                'data',
                reset ? data : [...photos[type].data, ...data]
            );
            Vue.set(
                photos[type],
                'offset',
                reset ? data.length : photos[type].offset + data.length
            );
            Vue.set(
                photos[type],
                'hasMore',
                data.length === photos[type].limit
            );
        }
    },
    updatePhotosLoader(state, { objectName, loaded }) {
        if (state.photosLoaded[objectName] !== undefined) {
            state.photosLoaded[objectName] = loaded;
        }
    }
};

const actions = {
    loadPhotos({ commit, state }, { objectName, type, userId, reset = false, man= false }) {
        const photos = state[objectName];
        if (!photos) return;

        const isAll = type === 'all';
        const limit = isAll ? state.generalSettings[objectName].limit : photos[type]?.limit || 0;
        const offset = isAll ? state.generalSettings[objectName].offset : reset ? 0 : (photos[type]?.offset || 0);

        const payload = {
            access_token: Vue.cookie.get('access_token'),
            user_id: userId,
            limit,
            offset,
            type,
        };

        if (!isAll) {
            photos[type].loader = true;
        }

        let postUrl = man ? API.getAllPhotosMan.url : API.getAllPhotos.url;

        return Vue.http
            .post(postUrl, payload)
            .then(response => response.json())
            .then(({ status, result }) => {
                if (status) {
                    commit('updatePhotos', { objectName, type, data: result, reset });
                    if (isAll) {
                        state.generalSettings[objectName].offset += state.generalSettings[objectName].limit;
                        commit('updatePhotosLoader', { objectName, loaded: true });
                    } else {
                        photos[type].loader = false;
                    }
                } else {
                    console.error('Ошибка на сервере');
                }
            })
            .catch(error => {
                console.error('Ошибка загрузки фото:', error);
            })
            .finally(() => {
                if (isAll) {
                    commit('updatePhotosLoader', { objectName, loaded: true });
                }
            });
    }
};

export default {
    state,
    getters,
    mutations,
    actions,
};
