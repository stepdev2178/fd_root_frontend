/**
 * Created by Alex Shvets on 23.08.2017.
 */


import Vue from 'vue';

const state = {
	mailModal: {
		to: {},
		open: false
	},
};

const getters = {
	mailModal( state ) {
		return state.mailModal;
	}
};

const mutations = {
	updateMailModal( state, payload ) {
		Object.assign( state.mailModal, payload );
	}
};

const actions = {
	loadMailUser(context, payload) {
		if (payload.to) {
			Vue.http.post('v2/profile/easy-search', {
				access_token: window.localStorage['access-token'],
				name_or_id: payload.to
			}).then(response => {
				context.commit('updateMailModal', {
					open: payload.open,
					to: response.body.result[0]
				})
			})
		} else {
			context.commit('updateMailModal', {
				open: payload.open,
				to: ''
			})
		}
	},
    manListDynamic(context, data) {
        return Vue.http.post('v2/male/man-list-dynamic', {
            access_token: Vue.cookie.get('access_token'),
            ...data
        })
            .then(data => data.json())
            .then(data => data)
	}
};


export default {
	state,
	getters,
	mutations,
	actions
}
