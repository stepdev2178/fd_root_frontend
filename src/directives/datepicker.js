// datepicker.directive.js
import moment from 'moment';
import 'moment/locale/ru';

export const datePickerDirective = {
    bind(el, binding, vnode) {
        // Initialize the date picker with default options
        const options = {
            useCurrent: true,
            format: 'DD.MM.YYYY',
            locale: 'ru',
            widgetPositioning: {
                horizontal: 'left'
            },
            ...binding.value // Allow overriding default options
        };

        // Initialize datetimepicker
        $(el).datetimepicker(options);

        // Handle date changes
        $(el).on('dp.change', (e) => {
            const date = e.date;
            const timestamp = moment(date).format('X');

            // Emit the value change event
            vnode.context.$emit('input', date._d);
            vnode.context.$emit('timestamp', timestamp);
        });
    },

    unbind(el) {
        // Cleanup
        $(el).off('dp.change');
        $(el).data('DateTimePicker').destroy();
    },

    update(el, binding) {
        // Update options if they change
        if (binding.value !== binding.oldValue) {
            $(el).data('DateTimePicker').options(binding.value);
        }
    }
};
