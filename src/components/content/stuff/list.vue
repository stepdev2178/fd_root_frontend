<template>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-blue box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">{{prettyTitle}} сервиса</h3>
                    <div class="box-tools pull-right">
                        <router-link :to="'/staff/create/' + $route.params.stuff">
                            <a class="btn btn-success">Создать <i class="fa fa-plus"></i></a>
                        </router-link>
                    </div>
                    <div class="box-tools pull-right">
                    </div>
                </div>
                <div class="box-body no-padding">
                    <form class="row">
                        <div class="form-group margin">
                            <div class="col-md-2">
                                <input type="text" class="form-control" placeholder="ID" v-model="searchId">
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" placeholder="ИФ" v-model="searchName">
                            </div>

                            <div class="col-md-2">
                                <select class="form-control placeholder-color" v-model="searchOnline">
                                    <option class="placeholder-option" selected="selected" value="">Онлайн</option>
                                    <option value="1">онлайн</option>
                                    <option value="0">офлайн</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <sst :loader="!loaded" @load="fetchData" :sort="sort" :data="data"
                         :count="count" :loadParams="loadParams" :order="order" :pagination="pagination">
                        <tr v-for="i in data" :key="i.id" slot="tbody">
                          <td>
                          <router-link :to="{name: 'Профиль сотрудника', params: {staff: i.id}}">
                            {{i.id}}
                          </router-link></td>
                            <td>{{i.name}}</td>
                            <td>
                                <span v-if="i.online" class="glyphicon glyphicon-ok-circle fa-2x text-green"></span>
                                <span v-else="" class="glyphicon glyphicon-remove-circle fa-2x text-red"></span>
                            </td>
                            <td>{{status(i.account_status)}}</td>
                            <td>{{date(i.created_at)}}</td>
                            <td><router-link :to="`/staff/edit/${$route.params.stuff}/${i.id}`" class="btn btn-primary btn-block edit-stuff"><i class="fa fa-edit fa-lg text-white"></i></router-link>
                            </td>
                        </tr>
                    </sst>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</template>

<script>
  import toastr from 'toastr'
  import _ from 'lodash'
  import moment from 'moment'
	import sst from '../../custom/serverSideTable.vue'
	import { SS_SORT_TABLE } from '@/mixins/utils.js'

    export default {
        name: 'AdminList',
        data() {
            return {
                loaded: false,
                data: [],
                table: null,
                searchId: '',
                searchName: '',
                searchOnline: '',
                searchStatus: '',
				sort: [
					{ active: true, name: 'id', column: 'ID' },
					{ active: false, name: 'name', column: 'ИФ' },
					{ active: false, name: 'online', column: 'Онлайн' },
					{ active: false, name: 'account_status', column: 'Статус' },
					{ active: false, name: 'created_at', column: 'Дата регистр.' },
					{ active: false, name: '', column: 'Действие' },
				],
            }
        },
		mixins: [ SS_SORT_TABLE ],
        watch: {
            searchId(value) {
                this.resetPagination();
				this.search()
            },
            searchName(value) {
                this.resetPagination();
				this.search()
            },
            searchOnline(value) {
                this.resetPagination();
				this.fetchData()
            },
            searchStatus(value) {
                this.resetPagination();
				this.fetchData()
            },
            '$route' (to, from) {
                this.resetPagination();
				this.fetchData()
            }
        },
        computed: {
            prettyTitle() {
                if (this.$route.params.stuff === 'admin') {
                    return 'Админы'
                } else if (this.$route.params.stuff === 'moderator') {
                    return 'Модераторы'
                }
            }
        },
        beforeMount() {
            this.fetchData();
        },
        methods: {
            fetchData() {
                this.loaded = false;
                this.$http.post('v2/site/staff/get/list', {
                    access_token: this.$cookie.get('access_token'),
                    staff_type: this.$route.params.stuff,
					account_status: JSON.stringify(['active','banned','deactivated']),
					offset: this.loadParams.offset,
					limit: this.loadParams.limit,
					name: this.searchName,
					online: this.searchOnline,
                    id: this.searchId
                }).then(data => {
                    if (data.body.status) {
                        this.data = data.body.result.list;
                        this.count = +data.body.result.count
                    } else {
                        toastr.error('Не могу получить список админов');
                        toastr.info(data.body.desc);
                    }


					this.loaded = true;
                }, err => {
                    toastr.error('Не могу получить список админов');
                    console.log(err);
                })

            },
			search: _.debounce( function () {
				this.fetchData()
			}, 300 ),
            status(status) {
				switch (status) {
					case 'active':
						return 'активный';
						break;
					case 'deactivated':
						return 'удаленный'
						break;
					case 'banned':
						return 'забаненый'
						break;
					case 'lock':
						return 'заблокированный';
						break;
				}
            },
            date(ts) {
                return moment(ts * 1000).format('LLL')
            },
        },
        created() {
			this.updOrderParams( {
				type: 'desc',
				by: 'id'
			} );
        },
        components: {
            sst
        }
    }
</script>
