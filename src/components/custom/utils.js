import toastr from 'toastr'
export default {
    constants: {
        imageSize: 2000000,
        docSize: 2000000 * 10,
        imageMimeTypes: ['image/jpeg', 'image/jpg', 'image/pjpeg', 'image/png'],
        documentMimeTypes: ['image/jpeg', 'image/jpg', 'image/pjpeg', 'application/pdf', ],
        videoMimeTypes: ['video/mp4','video/avi','video/m4v', 'video/quicktime', 'video/x-quicktime', 'video/x-m4v']

    },
    validateEmail(email)
    {
        let re = /\S+@\S+\.\S+/;
        return re.test(email);
    },
    checkImage(event) {
        return new Promise((resolve, reject) => {
            if(typeof event.target.files[0] === "undefined") {
                reject({
                    status: false,
                    description: 'Ошибка проверки изображения. Изображение не найдено'
                });
            }else {
                let image = event.target.files[0];
                if(image.size > this.constants.imageSize) {
                    reject({
                        status: false,
                        description: 'Превышен максимальный размер изображения'
                    })
                    return false;
                }
                let mimeTypeCheck = false;
                this.constants.imageMimeTypes.forEach(type => {
                    if(type === image.type) {
                        mimeTypeCheck = true;
                    }
                });
                if(mimeTypeCheck === false) {
                    reject({
                        status: false,
                        description: 'Неверное расширение изображения'
                    });
                    return false;
                }
                try {
                    let temporaryImage = new Image();
                    temporaryImage.onload = loadedImage => {
                        if(loadedImage.type === 'error') {
                            reject({
                                status: false,
                                description: 'Поврежденный файл'
                            });
                            return false;
                        }else {
                            let imgTarget = loadedImage.target;
                            if(imgTarget.naturalHeight > 3000 || imgTarget.naturalWidth > 3000) {
                                reject({
                                    status: false,
                                    description: 'Неверное разрешение фото (макс. разрешение 3000х3000 px)'
                                });
                                return false;
                            }else if (imgTarget.naturalHeight < 720 || imgTarget.naturalWidth < 720){
								reject({
									status: false,
									description: 'Неверное разрешение фото (мин. разрешение 720х720 px)'
								});
								return false;

                            }else {
                                resolve({
                                    status: true
                                });
                                return false;
                            }
                        }
                    };
                    temporaryImage.onerror = err => {
                        reject({
                            status: false,
                            description: 'Поврежденный файл!'
                        });
                        return false;
                    };
                    let temporaryImageReader = new FileReader();
                    temporaryImageReader.onload = readeImage => {
                        if(readeImage.type === 'error') {
                            reject({
                                status: false,
                                description: 'Поврежденный файл'
                            });
                            return false;
                        }else {
                            temporaryImage.src = readeImage.target.result;
                        }
                    };
                    temporaryImageReader.onerror = errReade => {
                        reject({
                            status: false,
                            description: 'Поврежденный файл'
                        });
                        return false;
                    };
                    temporaryImageReader.readAsDataURL(image);
                } catch (Ex) {
                    console.log(Ex);
                    reject({
                        status: false,
                        description: 'Поврежденный файл'
                    });
                    return false;
                }
            }
        })
    },

	checkDocument(event) {
		return new Promise((resolve, reject) => {
			if(typeof event.target.files[0] === "undefined") {
				reject({
					status: false,
					description: 'Файл не найдено'
				});
			}else {
				let image = event.target.files[0];
				if(image.size > this.constants.docSize) {
					reject({
						status: false,
						description: 'Превышен макс. размер документа 20Mb!'
					})
					return false;
				}
				let mimeTypeCheck = false;
				this.constants.documentMimeTypes.forEach(type => {
					if(type === image.type) {
						mimeTypeCheck = true;
					}
				});
				if(mimeTypeCheck === false) {
					reject({
						status: false,
						description: 'Неверное расширение документа!'
					});
					return false;
				}

			}
			resolve({
				status: true
			});
			return false;
		})
	},



    checkVideo(file) {
        return new Promise((resolve, reject) => {
            if(file.size > this.constants.docSize) {
                reject({
                	ok: false,
                    description: 'Превышен максимальный размер документа'
                });
				toastr.error( 'Превышен максимальный размер документа' );
            }else if(this.constants.videoMimeTypes.indexOf(file.type) === -1) {
                reject({
					ok: false,
                    description: 'Неверный формат документа'
                });
				toastr.error( 'Неверный формат документа' );
            } else {
                resolve({
                	ok: true,
                    description: 'Файл проверен'
                })
            }
        })
    }
}
