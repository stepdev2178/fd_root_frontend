import Vue from "vue";
import App from "./App";

import CONFIG from "./config/config";
import { API } from './config/API';
import Utils from "./components/custom/utils";

import router from "./router";
import store from "./store/store";

import VueCookie from "vue-cookie";
import VueResource from "vue-resource";
import VeeValidate from "vee-validate";
import * as pagination from  'vuejs-uib-pagination'
import vSelect from 'vue-select';
import VueSocketio from 'vue-socket.io';

import "vue-select/dist/vue-select.css";

import TabsPlugin from './plugins/tabs'

Vue.component('v-select', vSelect);

Vue.use( VeeValidate );
Vue.use( TabsPlugin );
Vue.use(pagination);
Vue.use( VueCookie );
Vue.use( VueResource );

Vue.config.productionTip = true;
Vue.prototype.utils = Utils;

Vue.prototype.config = CONFIG

Vue.use(VueSocketio, CONFIG.api.socket, store);

Vue.http.options.root = CONFIG.api.backend;
Vue.http.headers.Origin = 'example.com';
Vue.http.options.emulateJSON = true;
Vue.http.options.emulateHTTP = true;

Object.defineProperties(Vue.prototype,
    {
        '$bus': {
            get () {
                return this.$root.bus;
            }
        },
        '$main_config': {
            get() {
                return CONFIG;
            }
        }
    }
);

new Vue( {
	el: '#app',
	router,
    data: {
        bus: new Vue(),
        resource: Vue.resource(null, {}, API)
    },
	store,
	config,
    components: { App },
    render: (h) => h(App)
} );
