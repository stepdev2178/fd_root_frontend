import Vue from 'vue'
import Router from 'vue-router'
import store from '../store/store'

const Dashboard = () => import('../components/content/dashboard.vue')
const MissedInvites = () => import('../components/content/missedInvites/MissedInvites.vue')
const PartnersList = () => import('../components/content/partnersList.vue')
const WebmastersList = () => import('../components/content/webmaster/list.vue')
const CreateWebmaster = () => import('../components/content/webmaster/create.vue')
const WebmasterEdit = () => import('../components/content/webmaster/edit.vue')
const WebmasterView = () => import('../components/content/webmaster/view.vue')
const PartnerView = () => import('../components/content/partnerView.vue')
const WomenList = () => import('../components/content/girls/list-serverSide.vue')
const partnerEdit = () => import('../components/content/partner/edit.vue')
const CreateGift = () => import('../components/content/gifts/create.vue')
const GiftList = () => import('../components/content/gifts/list.vue')
const EditGift = () => import('../components/content/gifts/edit.vue')
const PriceList = () => import('../components/content/price/list.vue')
const PriceEdit = () => import('../components/content/price/edit.vue')
const CreatePartner = () => import('../components/content/partner/create.vue')
const PartnerTypes = () => import('../components/content/partner/type.vue')
const PartnerTypesEdit = () => import('../components/content/partner/type-edit.vue')
const CreatePartnerType = () => import('../components/content/partner/create-type.vue')
const CreditPackList = () => import('../components/content/credit-pack/list-serverSide.vue')
const EditCreditPack = () => import('../components/content/credit-pack/edit.vue')
const CreateCreditPack = () => import('../components/content/credit-pack/create.vue')
const Site404 = () => import('../components/main/404.vue')
const FeedbackGuest = () => import('../components/content/feedback/list-guest-serverSide.vue')
const FeedbackMan = () => import('../components/content/feedback/list-mans-serverSide.vue')
const FeedbackWoman = () => import('../components/content/feedback/list-women-serverSide.vue')
const FeedbackPartner = () => import('../components/content/feedback/list-partners-serverSide.vue')
const FeedbackAction = () => import('../components/content/feedback/action.vue')

const CreateVirtualGift = () => import('../components/content/virtual-gifts/create.vue')
const VirtualGiftList = () => import('../components/content/virtual-gifts/list.vue')
const EditVirtualGift = () => import('../components/content/virtual-gifts/edit.vue')
const PrivatePhotosServerSide = () => import('../components/content/moderation/private-photos-serverSide.vue')
const WomenOnAdminCreate = () => import('../components/content/partner/women-on-admin-create.vue')
const FineComponent = () => import('../components/content/settings/fine.vue')
const FineEditComponent = () => import('../components/content/settings/fine-edit.vue')
const SiteSetting = () => import('../components/content/settings/site-setting.vue')
const SiteSettingEdit = () => import('../components/content/settings/site-setting-edit.vue')
const UserProfile = () => import('../components/content/root/profile.vue')
const FinanceOrders = () => import('../components/content/finance/orders-serverSide.vue')
const FinanceFine = () => import('../components/content/finance/penalty-serverSide.vue')
const FinanceTransactions = () => import('../components/content/finance/transactions-serverSide.vue')
const Peplenishment = () => import('../components/content/finance/replenishment-updSelect.vue')
const PartnerAdmins = () => import('../components/content/partner/admin-list.vue')
const BranchesList = () => import('../components/content/partner/branches.vue')
const PartnersTrash = () => import('../components/content/partner/partners-trash.vue')
const CreateBranch = () => import('../components/content/partner/branch-create.vue')
const CreateFine = () => import('../components/content/finance/create-fine-updSelect.vue')
const CreateBonus = () => import('../components/content/finance/create-bonus-updSelect.vue')
const FinanceAccrual = () => import('../components/content/finance/accrual-updSelect.vue')
const FinancePaidOut = () => import('../components/content/finance/paid_out-updSelect.vue')

const AdminView = () => import('../components/content/partner/admin-view.vue')
const EditAutoTransaction = () => import('../components/content/finance/edit-auto.vue')
const EditManualTransaction = () => import('../components/content/finance/edit-manual-updSelect.vue')
const TransactionDetail = () => import('../components/content/finance/transaction-detail.vue')
const EditPartnerAdmin = () => import('../components/content/partner/admin-edit.vue')
const PartnerCreateWoman = () => import('../components/content/partner/woman-create.vue')
const CreatePartnerAdmin = () => import('../components/content/partner/admin-create.vue')
const WomanView = () => import('../components/content/girls/view.vue')
const WmnEditLandingPage = () => import('../components/content/girls/edit/landingPage.vue')
const WmnEditAdditionalInfo = () => import('../components/content/girls/edit/additionalInfo.vue')
const WmnEditAppearance = () => import('../components/content/girls/edit/appearance.vue')
const WmnEditContacts = () => import('../components/content/girls/edit/contacts.vue')
const WmnEditDocs = () => import('../components/content/girls/edit/docs.vue')
const WmnEditMain = () => import('../components/content/girls/edit/main.vue')
const WmnEditPhotos = () => import('../components/content/girls/edit/photos.vue')
const WmnEditVideos = () => import('../components/content/girls/edit/video.vue')
const WmnEditPrefers = () => import('../components/content/girls/edit/prefers.vue')
const WmnVirtualGiftView = () => import( '../components/content/girls/virtual-gift-view')

const BannedWoman = () => import('../components/content/girls/banned-list-serverSide.vue')
const OnModerationWoman = () => import('../components/content/girls/moderation-list-serverSide.vue')
const LockedWoman = () => import('../components/content/girls/locked-list-serverSide.vue')
const ManList = () => import('../components/content/man/list-serverSide.vue')
const MenLanding = () => import('../components/content/man/men-landing.vue')
const MenStatistic = () => import('../components/content/man/men-statistic.vue')
const MessagingHistory = () => import('../components/content/messagingHistory/MessagingHistory.vue')
const PhotoModeration = () => import('../components/content/moderation/photo-moderation-serverSide.vue')
const ProfileChangesModeration = () => import('../components/content/moderation/woman-info-serverSide.vue')
const PhotoDeleteModeration = () => import('../components/content/moderation/woman-photo-delete-serverSide.vue')
const AvatarModeration = () => import('../components/content/moderation/woman-avatar-serverSide.vue')
const ModerationNewWoman = () => import('../components/content/moderation/woman-new-serverSide.vue')
const ModerationNewWomanPage = () => import('../components/content/moderation/new-profile-moderation-page.vue')
const ModerationInfoWomanPage = () => import('../components/content/moderation/info-profile-moderation-page.vue')
const ModerationDelivery = () => import('../components/content/moderation/woman-delivery-serverSide.vue')
const DeliveryView = () => import('../components/content/moderation/delivery-view.vue')
const DeliveryEdit = () => import('../components/content/moderation/delivery-edit.vue')
const ModerationGiftList = () => import('../components/content/moderation/gift-list-serverSide.vue')
const ModerationGiftCatalog = () => import('../components/content/moderation/gift-catalog.vue')
const ModerationGiftLanding = () => import('../components/content/moderation/gifts-landing.vue')
const ModerationGiftView = () => import('../components/content/moderation/gift-view.vue')
const InfoRequestList = () => import('../components/content/moderation/info-request-list-serverSide.vue')
const InfoRequestModeration = () => import('../components/content/moderation/info-request-moderation.vue')

const WomanDistributions = () => import('../components/content/girls/distribution-serverSide.vue')
const WomanDistributionView = () => import('../components/content/girls/distribution-view.vue')
const WomanDistributionEdit = () => import('../components/content/girls/distribution-edit.vue')
const WomanDistributionCreate = () => import('../components/content/girls/distribution-create.vue')
const WomanLetterLog = () => import('../components/content/girls/letters-log-serverSide.vue')
const WomanLetterView = () => import('../components/content/girls/letter-view.vue')
const WomanLetterCreate = () => import('../components/content/girls/letter-create.vue')
const WomanGiftLog = () => import('../components/content/girls/gifts-list-serverSide.vue')
const WomanInfoRequest = () => import('../components/content/girls/info-request-list-serverSide.vue')
const WomanInfoRequestView = () => import('../components/content/girls/info-request-view.vue')
const WomanInTrash = () => import('../components/content/girls/trash-list-serverSide.vue')
const WomanVirtualLog = () => import('../components/content/girls/virtual-gift.vue')
const WomanGiftLanding = () => import('../components/content/girls/gifts-landing.vue')
const WomanGiftView = () => import('../components/content/moderation/gift-view.vue')

const WomanBotList = () => import('../components/content/girls/bot-list-serverSide.vue')
const WomanBotCreate = () => import('../components/content/girls/create-bot.vue')
const ManLetterLog = () => import('../components/content/man/letters-log-serverSide.vue')
const ManLetterView = () => import('../components/content/man/letter-view.vue')
const ManChatLog = () => import('../components/content/man/chat-log-serverSide.vue')
const ManCreate = () => import('../components/content/man/create.vue')
const ViewManProfile = () => import('../components/content/man/view.vue')
const EditManProfile = () => import('../components/content/man/edit.vue')
const ManBotList = () => import('../components/content/man/bot-list.vue')
const ManBotCreate = () => import('../components/content/man/bot-create.vue')
const BannedMan = () => import('../components/content/man/banned-serverSide.vue')
const TrashMan = () => import('../components/content/man/trash-serverSide.vue')

const BonusCategory = () => import('../components/content/finance/bonus-category-list.vue')
const EditBonusCategory = () => import('../components/content/finance/bonus-category-edit.vue')
const CreateBonusCategory = () => import('../components/content/finance/bonus-category-create.vue')

const StuffList = () => import('../components/content/stuff/list.vue')
const StuffTrashList = () => import('../components/content/stuff/trash.vue')
const StuffCreate = () => import('../components/content/stuff/create.vue')
const StaffShow = () => import('../components/content/stuff/view.vue')
const ModeratorStaffEdit = () => import('../components/content/stuff/edit-moderator.vue')
const AdminStaffEdit = () => import('../components/content/stuff/edit-admin.vue')

const GuestApi = () => import( '../components/content/promotion/guest-api.vue')
const Referal = () => import( '../components/content/promotion/referal.vue')
const MultiChat = () => import( '../components/content/multichat/index.vue')
// Video
const VideoList = () => import( '../components/content/girls/video-list.vue')
const DeleteVideo = () => import( '../components/content/moderation/video/DeleteVideo.vue')
const VideoPrivateModeration = () => import( '../components/content/moderation/video/VideoPrivateModeration.vue')
const VideoModeration = () => import( '../components/content/moderation/video/VideoModeration.vue')

const MultiChatSettings = () => import( '../components/content/settings/multichat.vue')

Vue.use(Router);

let router = new Router({
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    }
  },
  routes: [
    {
      path: '/',
      name: 'Панель',
      breadcrumb: 'Главная',
      meta: {
        denied: ['moderator'],
        access: ['admin', 'root'],
        rule: 'dashboard_access'
      },
      component: Dashboard,
      title: 'Главная',
    },
    {
      path: '/dashboard',
      name: 'Панель',
      breadcrumb: 'Главная',
      meta: {
        denied: ['moderator'],
        access: ['admin', 'root'],
        rule: 'dashboard_access'
      },
      component: Dashboard,
      title: 'Главная',
    },
    {
      path: '/partners',
      name: 'Список партнеров',
      breadcrumb: 'Список партнеров',
      component: PartnersList,
      title: 'Список партнеров',
      props: (route) => ({routeNew: +route.query.new, routeStatus: route.query.status}),
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/webmasters/list',
      name: 'Список вбмастеров',
      component: WebmastersList,
      meta: {
        rule: 'bot'
      }
    },
    {
      path: '/partner/trash',
      name: 'Корзина партнёров',
      breadcrumb: 'Корзина партнёров',
      component: PartnersTrash,
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/partners/view/:id',
      name: 'Просмотр партнера',
      breadcrumb: 'Просмотр партнера',
      component: PartnerView,
      title: 'Просмотр партнера',
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/webmaster/view/:id',
      name: 'Просмотр вебмастера',
      breadcrumb: 'Просмотр вебмастера',
      component: WebmasterView,
      title: 'Просмотр вебмастера',
      meta: {
        rule: 'access_webmaster'
      }
    },
    {
      path: '/partners/woman/create/:partner',
      name: 'Создание клиентки',
      component: PartnerCreateWoman,
      title: 'Просмотр партнера',

    },
    {
      path: '/partners/edit/:id',
      name: 'Редактирование партнера',
      breadcrumb: 'Редактирование партнера',
      component: partnerEdit,
      title: 'Редактирование партнера',
      meta: {
        rule: 'edit_agency'
      }
    },
    {
      path: '/webmaster/edit/:id',
      name: 'Редактирование вебмастера',
      breadcrumb: 'Редактирование вебмастера',
      component: WebmasterEdit,
      title: 'Редактирование вебмастера',
      meta: {
        rule: 'edit_webmaster'
      }
    },
    {
      path: '/women/list',
      name: 'Список клиенток',
      breadcrumb: 'Список клиенток',
      component: WomenList,
      title: 'Список клиенток',
      props: (route) => ({routeOnline: +route.query.online, routeWebcam: +route.query.webcam}),
      meta: {
        rule: 'access_woman',
        abbr: 'wmn-list'
      }
    },
    {
      path: '/services/gift/create',
      name: 'Создание подарка',
      breadcrumb: 'Создание подарка',
      component: CreateGift,
      title: 'Создание подарка',
      meta: {
        rule: 'setting_create_gift'
      }
    },
    {
      path: '/woman/bots',
      name: 'Клиентки боты',
      component: WomanBotList,
      meta: {
        rule: 'bot'
      }
    },
    {
      path: '/woman/bots/create',
      name: 'Создание бот-клиентки',
      component: WomanBotCreate,
      meta: {
        rule: 'bot'
      }
    },
    {
      path: '/services/gift/list',
      name: 'Сервисы - подарки',
      breadcrumb: 'Сервисы - подарки',
      component: GiftList,
      title: 'Сервисы - подарки',
      meta: {
        rule: 'setting_access_gift'
      }
    },
    {
      path: '/services/gift/edit/:id',
      name: 'Изменение подарка',
      breadcrumb: 'Изменение подарка',
      component: EditGift,
      title: 'Изменение подарка',
      meta: {
        rule: 'setting_edit_gift'
      }
    },
    {
      path: '/services/price/list',
      name: 'Список цен на сайте',
      breadcrumb: 'Список цен на сайте',
      component: PriceList,
      title: 'Список цен на сайте',
      meta: {
        rule: 'setting_access_price_list'
      }
    },
    {
      path: '/services/price/edit',
      name: 'Изменение цен на сайте',
      breadcrumb: 'Изменение цен на сайте',
      component: PriceEdit,
      title: 'Изменение цен на сайте',
      meta: {
        rule: 'setting_edit_price_list'
      }
    },
    {
      path: '/create/partner',
      name: 'Создание партнера',
      breadcrumb: 'Создание партнера',
      component: CreatePartner,
      title: 'Создание партнера',
      meta: {
        rule: 'create_agency'
      }
    },
    {
      path: '/create/webmaster',
      name: 'Создание вебмастера',
      breadcrumb: 'Создание вебмастера',
      component: CreateWebmaster,
      title: 'Создание вебмастера',
      meta: {
        rule: 'create_agency'
      }
    },
    {
      path: '/partner/admins',
      name: 'Список админов',
      breadcrumb: 'Список админов',
      component: PartnerAdmins,
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/partner-types',
      name: 'Типы партнеров',
      breadcrumb: 'Типы партнеров',
      component: PartnerTypes,
      title: 'Типы партнеров',
      meta: {
        rule: 'setting_access_agency_type'
      }
    },
    {
      path: '/partner-type-edit/:id',
      name: 'Изменить тип партнерства',
      breadcrumb: 'Изменить тип партнерства',
      component: PartnerTypesEdit,
      title: 'изменить тип партнерства',
      meta: {
        rule: 'setting_edit_agency_type'
      }
    },
    {
      path: '/create-agency-type',
      name: 'Создание нового типа агенств',
      breadcrumb: 'Создание нового типа агенств',
      component: CreatePartnerType,
      title: 'Новый тип агенства',
      meta: {
        rule: 'setting_create_agency_type'
      }
    },
    {
      path: '/credit-packs',
      name: 'Список кредит-паков',
      breadcrumb: 'Список кредит-паков',
      component: CreditPackList,
      title: 'Список кредит-паков',
      meta: {
        rule: 'setting_access_credit_pack'
      }
    },
    {
      path: '/credit-packs/edit/:id',
      name: 'Изменение кредит-пака',
      breadcrumb: 'Изменение кредит-пака',
      component: EditCreditPack,
      title: 'Изменение кредит-паков',
      meta: {
        rule: 'setting_edit_credit_pack'
      }
    },
    {
      path: '/credit-packs/create',
      name: 'Создание кредит-пака',
      breadcrumb: 'Создание кредит-пака',
      component: CreateCreditPack,
      title: 'Создание кредит-пака',
      meta: {
        rule: 'setting_create_credit_pack'
      },
    },
    {
      path: '/feedback/guest',
      name: 'Список обращений',
      breadcrumb: 'Список обращений',
      props: (route) => ({routeSort: +route.query.sort}),
      component: FeedbackGuest,
      meta: {
        rule: 'feedback_guest'
      },
    },
    {
      path: '/feedback/women',
      name: 'Список обращений от Клиенток',
      breadcrumb: 'Список обращений',
      component: FeedbackWoman,
      props: (route) => ({routeSort: +route.query.sort}),
      meta: {
        rule: 'access_feedback_woman'
      }
    },
    {
      path: '/feedback/partner',
      name: 'Список обращений от Партнеров',
      props: (route) => ({routeSort: +route.query.sort}),
      breadcrumb: 'Список обращений',
      component: FeedbackPartner,
      meta: {
        rule: 'feedback_agency'
      }
    },
    {
      path: '/feedback/mans',
      name: 'Список обращений от клиентов',
      props: (route) => ({routeSort: +route.query.sort}),
      breadcrumb: 'Список обращений',
      component: FeedbackMan,
      meta: {
        rule: 'feedback_man'
      }
    },
    {
      path: '/feedback/view/:id',
      name: 'Обращение от клиента',
      breadcrumb: 'Список обращений',
      component: FeedbackAction,
      meta: {
        rule: 'guest'
      }
    },
    {
      path: '/finance/orders',
      name: 'Бонусы',
      breadcrumb: 'Бонусы',
      component: FinanceOrders,
      meta: {
        rule: 'finance_access_bonus'
      }
    },
    {
      path: '/finance/penalty',
      name: 'Штрафы',
      breadcrumb: 'Штрафы',
      component: FinanceFine,
      props: (route) => ({routeSort: route.query.sort}),
      meta: {
        rule: 'finance_access_penalty'
      }
    },
    {
      path: '/finance/transactions',
      name: 'Транзакции',
      breadcrumb: 'Транзакции',
      component: FinanceTransactions,
      meta: {
        rule: 'finance_access_transaction'
      }
    },
    {
      path: '/finance/replenishment',
      name: 'Пополнения',
      breadcrumb: 'Пополнения',
      component: Peplenishment,
      meta: {
        rule: 'finance_access_refill'
      }
    },
    {
      path: '/finance/create/fine',
      name: 'Создать штраф',
      component: CreateFine,
      meta: {
        rule: 'finance_access_create_penalty'
      }
    },
    {
      path: '/finance/create/bonus',
      name: 'Создать бонус',
      component: CreateBonus,
      meta: {
        rule: 'finance_access_create_bonus'
      }
    },
    {
      path: '/finance/accrual',
      name: 'Начисления партнеров',
      component: FinanceAccrual,
      meta: {
        rule: 'finance_access_agency_accruals'
      }
    },
    {
      path: '/finance/paid_out',
      name: 'Выплаты партнеров',
      component: FinancePaidOut,
      meta: {
        rule: 'finance_access_agency_payments'
      }
    },
    {
      path: '/transaction/:id',
      name: 'Просмотр транзакции',
      component: TransactionDetail,
      meta: {
        rule: 'finance_access_transaction',
        breadcrumbs: [{
          name: 'Транзакции',
          link: '/finance/transactions'
        }]
      }
    },
    {
      path: '/transaction/edit/auto/:type/:id',
      name: 'Редактирование транзакции',
      component: EditAutoTransaction,
      meta: {
        rule: 'finance_access_transaction'
      }
    },
    {
      path: '/transaction/edit/manual/:type/:id',
      name: 'Редактирование транзакции',
      component: EditManualTransaction,
      meta: {
        rule: 'finance_access_transaction'
      }
    },
    {
      path: '/branches',
      name: 'Список филиалов',
      breadcrumb: 'Список филиалов',
      component: BranchesList,
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/branch/create/:parent',
      name: 'Создание филиала',
      breadcrumb: 'Создание филиала',
      component: CreateBranch,
      meta: {
        rule: 'create_agency'
      }
    },
    {
      path: '/partner/admin/show/:admin',
      name: 'Профиль админа',
      component: AdminView,
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/partner/admin/edit/:admin',
      name: 'Изменить админа',
      component: EditPartnerAdmin,
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/partner/admin/create/:partner',
      name: 'Создать админа партнеру',
      component: CreatePartnerAdmin,
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/partner/woman/create/admin/:admin',
      name: 'Создать клиентки под админом',
      component: WomenOnAdminCreate,
      meta: {
        rule: 'access_agency'
      }
    },
    {
      path: '/woman/view/:woman',
      name: 'Профиль клиентки',
      component: WomanView,
      meta: {
        rule: 'access_woman',
        abbr: 'wmn-profile'
      }
    },
    {
      path: '/woman/edit/:woman',
      component: WmnEditLandingPage,
      children: [
        {path: '', component: WmnEditMain, name: 'Изменение клиентки',},
        {path: 'additional', component: WmnEditAdditionalInfo, name: 'Изменение клиентки - Доп. инфо'},
        {path: 'appearance', component: WmnEditAppearance, name: 'Изменение клиентки - Внешность'},
        {path: 'contact', component: WmnEditContacts, name: 'Изменение клиентки - Контакты'},
        {path: 'docs', component: WmnEditDocs, name: 'Изменение клиентки - Документы'},
        {path: 'photos', component: WmnEditPhotos, name: 'Изменение клиентки - Фото'},
        {path: 'videos', component: WmnEditVideos, name: 'Изменение клиентки - Видео'},
        {path: 'prefers', component: WmnEditPrefers, name: 'Изменение клиентки - Предпочтения'},
      ],
      meta: {
        rule: 'edit_woman'
      }
    },
    {
      path: '/woman/banned',
      name: 'Забаненные девушки',
      component: BannedWoman,
      meta: {
        rule: 'access_woman'
      }
    },
    {
      path: '/woman/onmoderation',
      name: 'Клиентки новые',
      component: OnModerationWoman,
      meta: {
        rule: 'access_woman'
      }
    },
    {
      path: '/woman/locked',
      name: 'Заблокированные клиентки',
      component: LockedWoman,
      meta: {
        rule: 'access_woman'
      }
    },
    {
      path: '/woman/trash',
      name: 'Удаленные клиентки',
      component: WomanInTrash,
      meta: {
        rule: 'access_woman'
      }
    },
    {
      path: '/woman/distributions',
      name: 'Рассылки',
      component: WomanDistributions,
      meta: {
        rule: 'service_access_distribution'
      }
    },
    {
      path: '/woman/distribution/create',
      name: 'Создание рассылки',
      component: WomanDistributionCreate,
      meta: {
        rule: 'service_access_distribution'
      }
    },
    {
      path: '/woman/distribution/:id',
      name: 'Рассылка',
      component: WomanDistributionView,
      props: route => {
        return {
          moderation: true,
          id: +route.params.id
        }
      },
      meta: {
        rule: 'service_access_distribution'
      }
    },
    {
      path: '/woman/distribution/edit/:id',
      name: 'Редактирование рассылки',
      component: WomanDistributionEdit,
      props: route => {
        return {
          id: +route.params.id
        }
      },
      meta: {
        rule: 'service_edit_distribution'
      }
    },
    {
      path: '/woman/letters',
      name: 'Список писем',
      component: WomanLetterLog,
      meta: {
        rule: 'service_access_message'
      }
    },
    {
      path: '/woman/letter/:letter',
      name: 'Просмотр письма (клиентка)',
      component: WomanLetterView,
      meta: {
        rule: 'service_access_message'
      }
    },
    {
      path: '/woman/letters/create',
      name: 'Создание писем',
      component: WomanLetterCreate,
      meta: {
        rule: 'service_create_message'
      }
    },
    {
      path: '/man/gifts',
      name: 'Список подарков',
      props: (route) => ({routeCritical: +route.query.critical, routeNew: +route.query.new}),
      component: WomanGiftLog,
      meta: {
        rule: 'service_access_gift'
      }
    },
    {
      path: '/woman/gifts',
      name: 'Список подарков',
      props: (route) => ({routeCritical: +route.query.critical, routeNew: +route.query.new}),
      component: WomanGiftLog,
      meta: {
        rule: 'service_access_gift'
      }
    },
    // {
    // 	path: '/woman/gift/:gift',
    // 	name: 'Просмотр подарка',
    // 	component: WomanGiftLalala,
    // 	meta: {
    // 		rule: 'service_edit_gift'
    // 	}
    // },
    {
      path: '/woman/virtual-gifts',
      name: 'Список виртуальных подарков',
      props: (route) => ({routeCritical: +route.query.critical, routeNew: +route.query.new}),
      component: WomanVirtualLog,
      meta: {
        rule: 'service_access_virtual_gift'
      }
    },
    {
      path: '/man/inforequest',
      name: 'Список запросов контактной информации',
      props: (route) => ({routeCritical: +route.query.critical, routeNew: +route.query.new}),
      component: WomanInfoRequest,
      meta: {
        rule: 'service_access_realinfo_request_man'
      }
    },
    {
      path: '/inforequest',
      name: 'Список запросов контактной информации',
      props: (route) => ({routeCritical: +route.query.critical, routeNew: +route.query.new}),
      component: WomanInfoRequest,
      meta: {
        rule: 'service_access_realinfo_request'
      }
    },
    {
      path: '/inforequest/:request',
      name: 'Запрос контактной информации',
      component: WomanInfoRequestView,
      meta: {
        rule: 'service_access_realinfo_request'
      }
    },
    {
      path: '/woman/video-list',
      name: 'Список видео',
      component: VideoList,
      meta: {
        rule: 'access_woman'
      }
    },
    {
    	path: '/messaging-history',
    	name: 'История общения',
    	component: MessagingHistory,
        props: route => {
            return {
                time: route.params.time || 0,
                woman: route.params.woman || 0,
                man: route.params.man || 0
            }
        },
    	meta: {
    		rule: 'access_man'
    	}
    },
    {
      path: '/man/list',
      // name: 'Модерация подарков',
      component: MenLanding,
      meta: {
        rule: 'access_moderation_woman_gift_sent'
      },
      children: [
        {
          path: '/man/list',
          name: 'Список клиентов',
          props: (route) => ({
            routeNew: +route.query.new,
            routePay: +route.query.pay,
            routeOnline: +route.query.online,
            routeRNew: +route.query.rnew
          }),
          component: ManList,
          meta: {
            rule: 'access_man'
          }
        },
        {path: 'statistic', component: MenStatistic, name: 'Статистика клиентов'},
      ]
    },
    {
      path: '/man/create',
      name: 'Создание пользователя',
      component: ManCreate,
      meta: {
        rule: 'create_man'
      }
    },
    {
      path: '/man/view/:man',
      name: 'Профиль клиента',
      component: ViewManProfile,
      meta: {
        rule: 'access_man'
      }
    },
    {
      path: '/man/edit/:man',
      name: 'Изменение профиля клиента',
      component: EditManProfile,
      meta: {
        rule: 'edit_man'
      }
    },
    {
      path: '/man/letters',
      props: (route) => ({routeCritical: +route.query.critical || 0, routeNew: +route.query.new || 0}),
      name: 'Список сообщений',
      component: ManLetterLog
    },
    {
      path: '/man/letter/:letter',
      name: 'Просмотр сообщения',
      component: ManLetterView,
      meta: {
        rule: 'service_access_message'
      }
    },
    {
      path: '/man/bots',
      name: 'Клиенты-боты',
      component: ManBotList,
      meta: {
        rule: 'bot'
      }
    },
    {
      path: '/man/banned',
      name: 'Забаненные клиенты',
      component: BannedMan,
      meta: {
        rule: 'access_man'
      }
    },
    {
      path: '/man/trash',
      name: 'Удаленные клиенты',
      component: TrashMan,
      meta: {
        rule: 'access_man'
      }
    },
    {
      path: '/man/bot/create',
      name: 'Создать клиента бота',
      component: ManBotCreate,
      meta: {
        rule: 'bot'
      }
    },
    {
      path: '/chat-log',
      name: 'Логи чатов',
      component: ManChatLog,
      meta: {
        rule: 'service_access_chat_log'
      }
    },
    {
      path: '/moderation/photo',
      name: 'Модерация публичных фото',
      component: PhotoModeration,
      meta: {
        rule: 'access_moderation_woman_photo'
      }
    },
    {
      path: '/moderation/private-photos',
      name: 'Модерация приватных фото',
      component: PrivatePhotosServerSide,
      meta: {
        rule: 'access_moderation_woman_photo'
      }
    },
    {
      path: '/moderation/deletephoto',
      name: 'Модерация публичных фото на удаление',
      component: PhotoDeleteModeration,
      meta: {
        rule: 'access_moderation_photo_delete_request'
      }
    },
      // ========= Video ========
      {
          path: '/moderation/public-video',
          name: 'Модерация публичного видео',
          breadcrumb: 'Модерация приватного видео',
          component: VideoModeration
      },
      {
          path: '/moderation/private-video',
          name: 'Модерация приватного видео',
          breadcrumb: 'Модерация приватного видео',
          component: VideoPrivateModeration
      },
      {
          path: '/moderation/delete-video',
          name: 'Модерация удаления видео',
          breadcrumb: 'Модерация удаления видео',
          component: DeleteVideo
      },
    {
      path: '/moderation/distribution',
      name: 'Модерация рассылок',
      component: ModerationDelivery,
      meta: {
        rule: 'access_moderation_woman_distribution'
      }
    },
    {
      path: '/moderation/distribution/:id',
      name: 'Модерация рассылки',
      component: WomanDistributionView,
      props: route => {
        return {
          moderation: true,
          id: +route.params.id
        }
      },
      meta: {
        rule: 'access_moderation_woman_distribution'
      }
    },
    {
      path: '/moderation/delivery/edit/:delivery',
      name: 'Изменение рассылки',
      component: DeliveryEdit,
      meta: {
        rule: 'moderation_woman_distribution'
      }
    },
    {
      path: '/moderation/woman/new',
      name: 'Модерация новых клиенток',
      component: ModerationNewWoman,
      meta: {
        rule: 'access_moderation_woman_new_profile'
      }
    },
    {
      path: '/moderation/woman/new/moderation/:woman',
      name: 'Профиль новой клиентки',
      component: ModerationNewWomanPage,
      meta: {
        rule: 'access_moderation_woman_new_profile'
      }
    },
    {
      path: '/moderation/woman/info',
      name: 'Модерация изменений профилов',
      component: ProfileChangesModeration,
      meta: {
        rule: 'access_moderation_woman_change_profile'
      }
    },
    {
      path: '/moderation/woman/info/moderation/:moderation',
      name: 'Модерация изменения профиля',
      component: ModerationInfoWomanPage,
      meta: {
        rule: 'access_moderation_woman_change_profile'
      }
    },
    {
      path: '/moderation/avatar',
      name: 'Модерация аватаров',
      component: AvatarModeration,
      meta: {
        rule: 'access_moderation_woman_avatar'
      }
    },
    {
      path: '/moderation/gift',
      // name: 'Модерация подарков',
      component: ModerationGiftLanding,
      meta: {
        rule: 'access_moderation_woman_gift_sent'
      },
      children: [
        {path: '', component: ModerationGiftList, name: 'Модерация подарков'},
        {path: 'catalog', component: ModerationGiftCatalog, name: 'Каталог подарков'},
      ]
    },
    // {
    //   path: '/woman/gifts',
    //   // name: 'Модерация подарков',
    //   component: WomanGiftLanding,
    //   meta: {
    //     rule: 'access_moderation_woman_gift_sent'
    //   },
    //   children: [
    //     {path: '', component: WomanGiftLog, name: 'Подарки'},
    //     {path: 'catalog', component: WomanVirtualCatalog, name: 'Виртуальные подарки'},
    //   ]
    // },
    {
      path: '/woman/gift/:id',
      name: 'Просмотр подарка',
      component: ModerationGiftView,
      props: route => {
        return {
          moderation: true,
          id: +route.params.id
        }
      },
    },
    {
      path: '/woman/virtual-gifts/:id',
      name: 'Просмотр виртуального подарка',
      component: WmnVirtualGiftView
    },
    {
      path: '/moderation/info',
      name: 'Запрос контактной информации',
      component: InfoRequestList,
      meta: {
        rule: 'service_access_realinfo_request'
      }
    },
    {
      path: '/moderation/info/:request',
      name: 'Модерация запроса контактной информации',
      component: InfoRequestModeration,
      meta: {
        rule: 'service_edit_realinfo_request'
      }
    },
    {
      path: '/bonus-category',
      name: 'Категории бонусов',
      component: BonusCategory,
      meta: {
        rule: 'setting_access_bonus_category'
      }
    },
    {
      path: '/bonus-category/new',
      name: 'Создание категории',
      component: CreateBonusCategory,
      meta: {
        rule: 'setting_create_bonus_category'
      }
    },
    {
      path: '/bonus-category/:category',
      name: 'Изменение категории',
      component: EditBonusCategory,
      meta: {
        rule: 'setting_edit_bonus_category'
      }
    },
    {
      path: '/staff/:stuff',
      name: 'Список админов',
      component: StuffList,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/staff/trash/:stuff',
      name: 'Корзина персонала',
      component: StuffTrashList,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/staff/create/:stuff',
      name: 'Создание сотрудника',
      component: StuffCreate,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/staff/view/:staff',
      name: 'Профиль сотрудника',
      component: StaffShow,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/staff/edit/moderator/:user',
      name: 'Изменить модератора',
      component: ModeratorStaffEdit,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/staff/edit/admin/:user',
      name: 'Изменить админа',
      component: AdminStaffEdit,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/settings/fine',
      name: 'Настройки штрафов',
      component: FineComponent,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/settings/multichat',
      name: 'Настройки мультичата',
      component: MultiChatSettings,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/settings/fine/edit',
      name: 'Редактировать настройки штрафов',
      component: FineEditComponent,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/promotion/guest-api',
      name: 'Настройка гостевого API',
      desc: '',
      component: GuestApi,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/promotion/referal-links',
      name: 'Реферальные ссылки',
      desc: '',
      component: Referal,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/settings/service',
      name: 'Настройки сервиса',
      component: SiteSetting,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/settings/service/edit',
      name: 'Редактирование настроек сервиса',
      component: SiteSettingEdit,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/profile/root',
      name: 'Профиль',
      component: UserProfile,
      meta: {
        rule: 'root'
      }
    },
    {
      path: '/multichat',
      name: 'Мультичат',
      component: MultiChat,
      meta: {
        rule: 'root',
      },
    },
    {
      path: '/services/virtual-gift/create',
      name: 'Создание виртуального подарка',
      breadcrumb: 'Создание виртуального подарка',
      component: CreateVirtualGift,
      title: 'Создание виртуального подарка',
      meta: {
        rule: 'setting_create_virtual_gift'
      }
    },
    {
      path: '/services/virtual-gift/list',
      name: 'Сервисы - виртуальные подарки',
      breadcrumb: 'Сервисы - виртуальные подарки',
      component: VirtualGiftList,
      title: 'Сервисы - виртуальные подарки',
      meta: {
        rule: 'setting_access_virtual_gift'
      }
    },
    {
      path: '/services/virtual-gift/edit/:id',
      name: 'Изменение виртуального подарка',
      breadcrumb: 'Изменение виртуального подарка',
      component: EditVirtualGift,
      title: 'Изменение виртуального подарка',
      meta: {
        rule: 'setting_edit_virtual_gift'
      }
    },
    {
      path: '/missed-invites',
      name: 'Пропущенные инвайты',
      breadcrumb: 'Пропущенные инвайты',
      component: MissedInvites,
      title: 'Пропущенные инвайты',
    },
    {
      path: '*',
      name: 'Страница не найдена',
      breadcrumb: 'Создание филиала',
      component: Site404
    },
  ]
});

router.beforeEach((to, from, next) => {

  if (window.innerWidth < 767) {
    document.body.classList.remove('sidebar-open');
    if (document.querySelector('.slimScrollBar') !== null) {
      document.querySelector('.slimScrollBar').parentNode.removeChild(document.querySelector('.slimScrollBar'));
    }
  }

  $('.sidebar-menu li').each(function () {
    let link = $(this).find("a");
    if (link.attr("href").match(from.path)) {
      link.parent().removeClass("active");
      link.parent().parent().parent().removeClass("active");
    }
  });

  let app = store.getters.app;
  if (to.meta.rule === 'guest') {
    next();
  }
  if (app.user.accessLevel !== 'root') {
    let accessToRoute = to.meta.rule;
    let myRules = app.user.rules;
    if (myRules.indexOf(accessToRoute) !== -1) {
      // $('.sidebar-menu li').each(function(){
      //     let link = $(this).find("a");
      //     if(link.attr("href") == from.path){
      //         link.parent().removeClass("active");
      //         link.parent().parent().parent().removeClass("active");
      //     }
      // });
      document.title = to.name;
      next();
    } else {
      console.log('Access denied');
    }
  } else {
    document.title = to.name;
    next();
  }
});

router.afterEach((to, from) => {
  $('.sidebar-menu li').each(function () {
    let link = $(this).find("a");
    let b = false;
    for (let i = 0; i < link.length; i++) {
      if ($(link[i]).attr("href").match(to.path) && b !== true) {
        if (!b) {
          $(link[i]).parent().addClass("active");
          $(link[i]).parent().parent().parent().addClass("active");
        }
        b = true;
      }
    }
  });
});
export default router;
