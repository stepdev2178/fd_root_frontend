let domain = import.meta.env.VITE_DOMAIN;
let productionMode = import.meta.env.VITE_PRODUCTION === 'true';

export default {
  prod: productionMode,
  media: {
    admin: '',
    frontend: ''
  },
  api: {
    backend: `https://apiadminpanel.${domain}`,
    frontend: `https://api.${domain}`,
    socket: `https://chat.${domain}`,
  },
  siteUrl: `https://${domain}`
};
