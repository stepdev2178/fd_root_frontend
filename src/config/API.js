export const API = {
    // проверка токена
    checkAccessToken: {
        method: 'POST',
        url: 'v2/authorization/verify-token'
    },
    // проверка токена
    logInAdmin: {
        method: 'POST',
        url: 'v2/authorization/login-user'
    },
    // загрузить дашбоард
    getDashboard: {
        method: 'POST',
        url: 'v2/dashboard/get/statistic'
    },
    getCountries: {
        method: 'POST',
        url: 'v2/guest/get-country'
    },
    getCities: {
        method: 'POST',
        url: 'v2/guest/get-city'
    },
    // --------- FINANCES ---------
    getFinanceBonuses: {
        method: 'POST',
        url: 'v2/bonuses/get/bonus'
    },
    getFinesCategory: {
        method: 'POST',
        url: 'v2/bonuses/load-categories'
    },
    getFinanceFines: {
        method: 'POST',
        url: 'v2/bonuses/load-penalty'
    },
    getFinancePayments: {
        method: 'POST',
        url: 'v2/bonuses/get/paid-out'
    },
    // --------- SERVICES ---------
    getSentLetters: {
        method: 'POST',
        url: 'v2/agency/service/letter/sent'
    },
    createMessage: {
        method: 'POST',
        url: 'v2/agency/service/create/letter'
    },
    getManListDynamic: {
        method: 'POST',
        url: 'v2/agency/service/dynamic-search/male-list'
    },
    getManList: {
        method: 'POST',
        url: 'v2/agency/service/get/male-list'
    },
    getInbox: {
        method: 'POST',
        url: 'v2/agency/service/letter/inbox'
    },
    getDistributions: {
        method: 'POST',
        url: 'v2/agency/service/distributions'
    },
    getGifts: {
        method: 'POST',
        url: 'v2/agency/service/gift/get-all'
    },
    getRealinfoRequest: {
        method: 'POST',
        url: 'v2/agency/service/get/list-realinfo-request'
    },
    sentMessageReply: {
        method: 'POST',
        url: 'v2/agency/service/letter/reply'
    },
    getGroupedPhotos: {
        method: 'POST',
        url: 'v2/female/load-photos'
    },
    // --------- LADIES ---------
    // просмотр девушки
    createLady: {
        method: 'POST',
        url: 'v2/female/create'
    },
    // просмотр девушки
    viewLady: {
        method: 'POST',
        url: 'v2/female/detail'
    },
    // загрузить фотки
    getLadyPhotos: {
        method: 'POST',
        url: 'v2/female/load-photos'
    },
    getAllPhotos: {
        method: 'POST',
        url: 'v2/female/woman-photos'
    },
    getAllPhotosMan: {
        method: 'POST',
        url: 'v2/male/man-photos'
    },
    // загрузить документы девушки
    getLadyDocuments: {
        method: 'POST',
        url: 'v2/female/document/search'
    },
    setMainAvatar: {
        method: 'POST',
        url: 'v2/female/establish/avatar'
    },
    setAvatarFromModerated: {
        method: 'POST',
        url: 'v2/female/establish/moderated-avatar'
    },
    makeBan: {
        method: 'POST',
        url: 'v2/agency/service/get/baned'
    },
    toModeration: {
        method: 'POST',
        url: 'v2/female/move-to-moderation'
    },
    changePassword: {
        method: 'POST',
        url: 'v2/agency/staff/establish/password'
    },
    // загрузка девушек
    getLadies: {
        method: 'POST',
        url: 'v2/femal/load-list'
    },
    // загрузить новых девушек
    getNewLadies: {
        method: 'POST',
        url: 'v2/femal/get/new'
    },
    // загрузить измененных девушек
    getChangesLadies: {
        method: 'POST',
        url: 'v2/femal/get/profile-changes'
    },
    // загрузить забаненых девушек
    getBannedLadies: {
        method: 'POST',
        url: 'v2/femal/get/banned'
    },
    // загрузить залоченых девушек
    getLockedLadies: {
        method: 'POST',
        url: 'v2/femal/get/locked'
    },
    // загрузить неактивных девушек
    getDeactivatedLadies: {
        method: 'POST',
        url: 'v2/femal/get/deactivated'
    },
    deleteLadyPhoto: {
        method: 'POST',
        url: 'v2/female/remove-photo'
    },
    uploadLadyPhoto: {
        method: 'POST',
        url: 'v2/female/upload-photo'
    },
    uploadLadyDocument: {
        method: 'POST',
        url: 'v2/female/document/upload'
    },
    deleteLadyDocument: {
        method: 'POST',
        url: 'v2/female/document/disable'
    },
    editLadyMainInfo: {
        method: 'POST',
        url: 'v2/female/edit-main-info'
    },
    editLadyContacts: {
        method: 'POST',
        url: 'v2/female/edit-personal-contact'
    },
    editLadyAppearance: {
        method: 'POST',
        url: 'v2/female/edit-user-appearance'
    },
    editLadyAdditional: {
        method: 'POST',
        url: 'v2/female/edit-additional-info'
    },
    editLadyPreferences: {
        method: 'POST',
        url: 'v2/female/edit-user-preference'
    },
    // Видео девушки
    uploadLadyVideo: {
        method: 'POST',
        url: 'v2/female/upload-video'
    },
    uploadLadyVideoPreview: {
        method: 'POST',
        url: 'v2/female/upload-video-preview'
    },
    deleteLadyVideo: {
        method: 'POST',
        url: 'v2/female/remove-video'
    },
    getLadyVideos: {
        method: 'POST',
        url: 'v2/female/load-videos'
    },

    // --------- ADMINS ---------
    // загрузить админов
    getAdmins: {
        method: 'POST',
        url: 'v2/agency/staff/get/admins'
    },
    getRelatedWomanAdmin: {
        method: 'POST',
        url: 'v2/agency/staff/get/related-woman'
    },
    getBranches: {
        method: 'POST',
        url: 'v2/agency/staff/get/branches'
    },
    // просмотр одного админа
    viewAdmin: {
        method: 'POST',
        url: 'v2/agency/staff/get/admin-info'
    },
    adminLadies: {
        method: 'POST',
        url: 'v2/agency/staff/get/related-woman'
    },
    adminDocs: {
        method: 'POST',
        url: 'v2/female/document/search'
    },
    createAdmin: {
        method: 'POST',
        url: 'v2/agency/staff/create/admin'
    },
    editAdminMainInfo: {
        method: 'POST',
        url: 'v2/agency/staff/update/main-admin'
    },
    editAdminContacts: {
        method: 'POST',
        url: 'v2/agency/staff/update/admin-contacts-info'
    },
    uploadAdminDocument: {
        method: 'POST',
        url: 'v2/female/document/upload'
    },
    deleteAdminDocument: {
        method: 'POST',
        url: 'v2/female/document/disable'
    },
    // --------- MODERATIONS ---------
    // загрузить админов
    getModPrivatesPhotos: {
        method: 'POST',
        url: 'v2/agency/moderation/get/private-photos'
    },
    saveGiftMessageEdit: {
        method: 'POST',
        url: 'v2/agency/service/edit/gift-message'
    },
    getModGifts: {
        method: 'POST',
        url: 'v2/agency/moderation/get/present'
    },
    getModContacts: {
        method: 'POST',
        url: 'v2/agency/moderation/get/realinfo-request'
    },
    acceptPrivatePhoto: {
        method: 'POST',
        url: 'v2/agency/moderation/accept/private-photo'
    },
    rejectPrivatePhoto: {
        method: 'POST',
        url: 'v2/agency/moderation/reject/private-photo'
    },
    acceptModeratedGift: {
        method: 'POST',
        url: 'v2/agency/service/gift/accept'
    },
    rejectModeratedGift: {
        method: 'POST',
        url: 'v2/agency/service/gift/reject'
    },
    deleteAttachmentFromGiftMessage: {
        method: 'POST',
        url: 'v2/agency/service/remove/gift-message-attachment'
    },
    // ---------- FEEDBACK ----------------
    feedback: {
        method: 'POST',
        url: 'v2/feedback/user-sent'
    },
    // --------- ACTIONS_BTN ---------
    getDetailedMessage: {
        method: 'POST',
        url: 'v2/agency/service/letter/one'
    },
    getDetailedDistribution: {
        method: 'POST',
        url: 'v2/agency/distribution/one'
    },
    getDetailedGift: {
        method: 'POST',
        url: 'v2/agency/service/gift/one'
    },
    getDetailedRealInfo: {
        method: 'POST',
        url: 'v2/agency/service/get/one-request-realinfo'
    },
    // COUNTERS
    getModerationCounters: {
        method: 'POST',
        url: 'v2/agency/moderation/counter'
    },
    getServicesCounters: {
        method: 'POST',
        url: 'v2/agency/service/counter'
    },
    getWomanCounters: {
        method: 'POST',
        url: 'v2/female/counter'
    },
    // OTHERS
    getBlackList: {
        method: 'POST',
        url: 'v2/agency/distribution/blacklist-load'
    },
    searchMansByValue: {
        method: 'POST',
        url: 'v2/male/user-search'
    },
    createDistribution: {
        method: 'POST',
        url: 'v2/agency/distribution/create'
    },
    editDistribution: {
        method: 'POST',
        url: 'v2/agency/distribution/edit'
    },
    acceptRealInfo: {
        method: 'POST',
        url: 'v2/agency/service/accept/realinfo-request'
    },
    rejectRealInfo: {
        method: 'POST',
        url: 'v2/agency/service/reject/realinfo-request'
    },
    agencyChatLogs: {
        method: 'POST',
        url: 'v2/agency/service/chat-logs'
    }

};
