var App = function() {

    // Отображение placeholder для IE9
    var handleFixInputPlaceholderForIE = function() {
        //fix html5 placeholder attribute
        if (!!navigator.userAgent.match(/MSIE 9.0/)) { // ie9
            // this is html5 placeholder fix for inputs, inputs with placeholder-no-fix class will be skipped(e.g: we need this for password fields)
            $('input[placeholder]:not(.placeholder-no-fix), textarea[placeholder]:not(.placeholder-no-fix)').each(function() {
                var input = $(this);

                if (input.val() === '' && input.attr("placeholder") !== '') {
                    input.addClass("placeholder").val(input.attr('placeholder'));
                }

                input.focus(function() {
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                    }
                });

                input.blur(function() {
                    if (input.val() === '' || input.val() == input.attr('placeholder')) {
                        input.val(input.attr('placeholder'));
                    }
                });
            });
        }
    };

    // placeholder для input и select
    var handleHidePlaceholder = function() {

            $("[placeholder]").each(function() {
                $(this)
                .data("holder", $(this).attr("placeholder"))
                .focusin(function(){
                    $(this).attr('placeholder','');
                })
                .focusout(function(){
                    $(this).attr('placeholder',$(this).data('holder'));
                });
            });

            $( "select" ).change(function() {
                $( "select option:selected" ).each(function() {
                  if($( this ).hasClass("placeholder-option")){
                    $( this ).parent().addClass("placeholder-color");
                  }
                  else{
                    $( this ).parent().removeClass("placeholder-color");
                  }
                });
              }).trigger( "change" );

    };

    // Показать подтаблицу по клику
    var handleSubTable = function() {
        // Обработчик нажатия на ячейку со стрелкой
        $(".btn-subtable").each(function(){
            var subTabl = $(this).parent().next();
            var subTablWrap = subTabl.find(".sub-table-wrapp");

            $(this).on("click", function(){

                // скрываем все кроме текущей
                $(".btn-subtable").not($(this)).removeClass("open-sub").removeClass("bg-primary");
                $(".btn-subtable").not($(this)).find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up");
                $(".btn-subtable").not($(this)).parent().next().find(".sub-table-wrapp").slideUp(function() {
                    $(this).parent().parent().hide();
                });
                $(".btn-subtable").not($(this)).removeClass("bg-primary");

                // если открыта скрываем
                if($(this).hasClass("open-sub")){
                    $(this).removeClass("open-sub").removeClass("bg-primary");
                    $(this).find(".fa").removeClass("fa-angle-down").addClass("fa-angle-up");
                    subTablWrap.slideUp(function() {
                        subTabl.hide();
                    });
                    $(this).removeClass("bg-primary");
                }else{
                    subTabl.show();
                    subTablWrap.slideDown();
                    $(this).addClass("open-sub");
                    $(this).find(".fa").removeClass("fa-angle-up").addClass("fa-angle-down");
                    $(this).addClass("bg-primary");
                }
            });
        });
    };

    // Стильный чекбокс iCheck
    var handleiCheck = function() {
        $('input[type=checkbox], input[type=radio]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    };

    // Селект select2
    var handleSelect2 = function() {
        // без поля поиска
        $('.select2').select2({
            placeholder: $(this).attr("data-placeholder"),
            width: '100%',
            minimumResultsForSearch: -1
        });

        // с полем поиска
        $('.select-search').select2({
            placeholder: $(this).attr("data-placeholder"),
            width: '100%'
        });

        $('.select-big').select2({
            placeholder: false,
            minimumResultsForSearch: Infinity,
            width: '250'
        });

        $('.select-midd').select2({
            placeholder: false,
            minimumResultsForSearch: Infinity,
            width: '180'
        });
    };


    // фильтр по дате, дне, месяце
    var handleFilterDatepicker = function() {
        var fromSort = $(".from-sort"), toSort = $(".to-sort");

        // перед submit формы изменяем value
        $("#filter-main-info").submit(function(event) {
           event.preventDefault();

          $(this).find("[server-val]").each(function(){
            $(this).val($(this).attr("server-val"));
          });

          this.submit();
        });

        // вставка даты для отображения и передачи на сервер в input
        function setUnixData(d, valD){
            var unixVal = d.unix();
            valD.val(d.format('DD.MM.YYYY HH:mm'));
            // UNIX формат
            valD.attr("server-val", unixVal);
        };

        // Дата по умолчанию
        var _start = moment().set('date', 1).set('hour', 00).set('minute', 00);
        var _end = moment();
        setUnixData(_start, fromSort);
        setUnixData(_end, toSort);

        function _filterDate(i){
            switch (i) {
              case "0":
                // Текущих месяц
                _start = moment().set('date', 1).set('hour', 00).set('minute', 00);
                _end = moment();
                setUnixData(_start, fromSort);
                setUnixData(_end, toSort);
                break;
              case "1":
                // Прошлый месяц
                _start = moment().set('month', (moment().get('month') - 1)).set('date', 1).set('hour', 00).set('minute', 00);
                _end = moment().set('month', moment().get('month')).set('date', 0).set('hour', 23).set('minute', 59);
                setUnixData(_start, fromSort);
                setUnixData(_end, toSort);
                break;
              case "2":
                // Вчера
                _start = moment().subtract(1, 'day').set('hour', 00).set('minute', 00);
                _end = moment().subtract(1, 'day').set('hour', 23).set('minute', 59);
                setUnixData(_start, fromSort);
                setUnixData(_end, toSort);
                break;
              case "3":
                // Сегодня
                _start = moment().set('hour', 00).set('minute', 00);
                _end = moment();
                setUnixData(_start, fromSort);
                setUnixData(_end, toSort);
                break;
              default:
                console.log("Eorror in handleSortDatepicker");
            }
        };

        $('.filter-date-nav').on('change', function() {
            var valSelected = $(this).find(":selected").val();
            _filterDate(valSelected);
        });

    };

    // Дата и врмя bootstrap widget
    var handleDatepicker = function() {
        // дата
        $('.d-picker').datetimepicker({
            useCurrent: false,
            format: 'DD.MM.YYYY',
            locale:'ru',
            widgetPositioning: {
                horizontal: "left"
            }
        });

        // время и дата одновременно
        $(".d-t-picker").datetimepicker({
            useCurrent: false,
            format: 'DD.MM.YYYY H:mm',
            locale:'ru',
            widgetPositioning: {
                horizontal: "left"
            }
        });
    };

    // Плагин просмотра фоток
    var handlePhotoViewer = function(){
         $('.photo-nav').lightGallery({
            download : true,
            thumbWidth: 128,
            thumbContHeight: 128,
            thumbnail:true
        });
    };

    // шаблон номера телефона
    var handleInputMask = function(){
        $("[data-mask]").inputmask();
    };

    // Кнопка подняться вверх по стр
    var handleScrollToTop = function(){
        var  topLink = $(".page-top");

       topLink.on("click", function(){
            App.scrollTo();
        });

        $(window).scroll(function (){

            if($(this).scrollTop() > 200)
            {
                topLink.fadeIn();
            }
            else
            {
                topLink.fadeOut();
            }

            });
    };

    // Стильный скролл бар
    var handleScroll= function(){
        // table scroll
        $(".table").each(function(){
            $(this).not('.subtable').wrap('<div class="h-scroll"></div>');
        });

        // скролл инициализация
        $('.modal').each(function(){
            $(this).on('shown.bs.modal', function (e) {
              $(this).find(".v-scroll").perfectScrollbar('update');
            });
        });

        // вертикальный скролл
        $(".v-scroll").each(function(){

            if($(this).attr("data-height")){
                $(this).css("height", $(this).attr("data-height")+"px");
            }
            $(this).perfectScrollbar();
        });

        // горизонтальный скролл
        $(".h-scroll").each(function(){
            $(this).perfectScrollbar();
        });
    };

    // Handles Bootstrap Modals.
    var handleModals = function() {
        // fix stackable modal issue: when 2 or more modals opened, closing one of modal will remove .modal-open class.
        $('body').on('hide.bs.modal', function() {
            if ($('.modal:visible').length > 1 && $('html').hasClass('modal-open') === false) {
                $('html').addClass('modal-open');
            } else if ($('.modal:visible').length <= 1) {
                $('html').removeClass('modal-open');
            }
        });

        // fix page scrollbars issue
        $('body').on('show.bs.modal', '.modal', function() {
            if ($(this).hasClass("modal-scroll")) {
                $('body').addClass("modal-open-noscroll");
            }
        });

        // fix page scrollbars issue
        $('body').on('hidden.bs.modal', '.modal', function() {
            $('body').removeClass("modal-open-noscroll");
        });

        // remove ajax content and remove cache on modal closed
        $('body').on('hidden.bs.modal', '.modal:not(.modal-cached)', function () {
            $(this).removeData('bs.modal');
        });
    };

    // Показать модальное окно с подтверждением лицензии
    var handleAutoShowInfoInModal= function(){
        if($(".help-popup-licence").length > 0) // если на странице есть класс .help-popup-licence
        {
            $(".help-modal-link").attr("data-target", "#help-view-modal-index");

            // время авто-появления модалки
            var timeShow = 1000;
            var closeFlag = false;
            var modalBlock = $('#help-view-modal-index');
            var contScroll = $('#help-view-modal-index .v-scroll');

            // если он не прочитал соглашение, выводим модалку с текстом соглашения
            if(!localStorage.getItem("adminLis") || localStorage.getItem("adminLis") === "no")
            {

                setTimeout(function(){
                    modalBlock.modal({show:true});
                }, timeShow);

                // запрет закрытия
                modalBlock.on('hide.bs.modal', function(e){
                    if(!closeFlag) {
                        e.preventDefault();
                        e.stopImmediatePropagation();
                        return false;
                    }
                });

                contScroll.on("ps-y-reach-end", function(){
                    setTimeout(function(){
                        $("#help-view-modal-index .modal-footer").fadeIn();
                    }, 1000);
                });

                // запомнить что пользователь соглаасен и не желает больше видить модалку (если нужно отправить на сервер метку)
                $(".yes-agree").on("click", function(){
                    closeFlag = true;
                    // ставим метку что он прочитал
                    localStorage.setItem("adminLis", "yes");
                    modalBlock.modal("hide");
                });

            }
            else{
                $("#help-view-modal-index .modal-footer").show();
                $(".yes-agree").on("click", function(){
                    localStorage.setItem("adminLis", "yes");
                    modalBlock.modal("hide");
                });
            }
        }
    };

    // загрузить файлы с просмотром и удалением
    var handleFileUploader = function(){

        // function readURL(input, element) {
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();
        //
        //         reader.onload = function (e) {
        //             element.attr('src', e.target.result);
        //         }
        //
        //         reader.readAsDataURL(input.files[0]);
        //     }
        // };

        // загрузчик автара
        $("#part-ava-load-input").change(function(){
            readURL(this, $('#part-avatar'));
        });
        // удалить авку
        $(".ava-load-wrapp .del-photo").on("click", function(){

            App.confirm(
                "Вы действительно хотите удалить?",
                function(t){
                    if(t)
                    {
                       var newSrc = $('#part-avatar').attr("src-default");
                        $('#part-avatar').attr("src", newSrc);
                        App.notify("Аватар успешно удален!", "success");
                    }
            });
        });

        // загрузчик отдельного файла
        $(".load-file-input").change(function(){
            readURL(this, $('.load-file-src'));
        });
        // удалить файл
        $(".del-photo").not(".ava-load-wrapp .del-photo").on("click", function(){

            App.confirm(
                "Вы действительно хотите удалить?",
                function(t){
                    if(t)
                    {
                       var newSrc = $('.load-file-src').attr("src-default");
                        $('.load-file-src').attr("src", newSrc);
                        App.notify("Файл успешно удален!", "success");
                    }
            });
        });

        // // загрузить или удалить фотку в списке
        // $(".file-loader").change(function(){
        //     if (this.files && this.files[0]) {
        //         var navWrap = $(this).attr("data-nav");
        //         var reader = new FileReader();
        //         reader.onload = function (e) {
        //
        //             var neWItem = '<a href="'+e.target.result+'">\
        //                   <img src="'+e.target.result+'">\
        //                   <span class="fa fa-times-circle del-photo" photo-id="'+App.getUniqueID()+'"></span>\
        //                 </a>';
        //
        //             $(navWrap).prepend(neWItem);
        //             setTimeout(function(){
        //                 eventDelP(navWrap);
        //             }, 500);
        //         }
        //         reader.readAsDataURL(this.files[0]);
        //     }
        // });

        // передвижение по табам (step 1 ...)
        $('.btnNext').click(function(){
        	$('.nav-tabs > .active').next('li').find('a').trigger('click');
        });

        // удалить фото
        function deletePhoto(photo, id){
            photo.parent().remove();
        };

        // обработка событий
        function eventDelP(navUpload){
            $(navUpload).data("lightGallery").destroy(true);
            $(navUpload).lightGallery({
                download : true,
                thumbWidth: 128,
                thumbContHeight: 128,
                thumbnail:true
            });

            $(".photo-nav .del-photo").on("click", function(){
                var _thisPhoto = $(this);
                App.confirm(
                    "Вы действительно хотите удалить?", function(t){
                    if(t)
                    {
                       deletePhoto(_thisPhoto, _thisPhoto.attr("photo-id"));
                       App.notify("Успешно удален!", "success");
                    }
                });
                return false;
            });
        };

        $(".photo-nav .del-photo").on("click", function(){
                var _thisPhoto = $(this);
                App.confirm(
                    "Вы действительно хотите удалить?", function(t){
                    if(t)
                    {
                       deletePhoto(_thisPhoto, _thisPhoto.attr("photo-id"));
                       App.notify("Успешно удален!", "success");
                    }
                });
                return false;
            });

    };

    // открыть bootstrap tab из url
    var handleTabActive = function(){
        if($(".nav-tabs").length > 0)
        {
            if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');

            $('a[data-toggle="tab"]').on('shown', function(e) {
              return location.hash = $(e.target).attr('href').substr(1);
            });

            $('a[data-toggle="tab"]').on('click', function(e) {
              return location.hash = $(e.target).attr('href').substr(1);
            });
        }
    };

    // делаем активным пункт меню
    var handleMenuItemActive = function(){

        // получить url страгници без доменного имени
        var urlPage = document.location.pathname + document.location.search
            urlPage = urlPage.substr(1);


        // сделать активным пункт меню и его раздел, если url страницы совпадает с href ссылки в меню (без учета домена)
        $('.sidebar-menu li').each(function(){
            var link = $(this).find("a");
            if(link.attr("href") === urlPage){
                link.parent().addClass("active");
                link.parent().parent().parent().addClass("active");
            }
        });

        // сделать активным пункт меню по требоваию (hard)
        function setActiveMenuLink(linkHref){
            $('.sidebar-menu li').each(function(){
                var link_st = $(this).find("a");

                if(link_st.attr("href") === linkHref){
                    link_st.parent().addClass("active");
                    link_st.parent().parent().parent().addClass("active");
                }
            });
        };

        // условия для определенных страниц (hard link)
        switch(urlPage){
            case "woman_view.php":
            case "woman_create.php":
            case "woman_edit.php":
                // сделать активной передаваемую ссылку
                setActiveMenuLink("women_list.php");
            break;

            case "partner_view.php":
            case "partner_edit.php":
            case "partner_create.php":
                // сделать активной передаваемую ссылку
                setActiveMenuLink("partner_list.php");
            break;

            case "bonus_category_edit.php":
            case "bonus_category_create.php":
                // сделать активной передаваемую ссылку
                setActiveMenuLink("settings_bonus_category.php");
            break;

            case "cost_edit.php":
                // сделать активной передаваемую ссылку
                setActiveMenuLink("services_cost.php");
            break;
        }

    };


    // ajax select
    var handleAjaxSelect = function(){
        if($(".search-input").length > 0)
        {

            var inputS = $(".search-input"),
            inputSearchList = $(inputS.attr("data-dropdown-id")),
            insIdBl = $(inputS.attr("data-insert-id"));

            inputS.focus(function() {
                inputSearchList.fadeIn();
                 inputSearchList.find(".v-scroll").perfectScrollbar('update');
                 inputS.parent().parent().next().css("opacity", "0.2");

                 inputSearchList.find(".fa-plus").each(function(){
                    $(this).on("click", function(){

                        var itemL = $(this).parent().parent().parent().clone();
                        itemL.find(".fa").removeClass("fa-plus").addClass("fa-times");
                        itemL.find(".label").removeClass("label-success").addClass("label-warning");
                        insIdBl.prepend(itemL);

                        initHideItems(inputSearchList);

                        initRemoveItem(".products-list .fa-times");

                        return false;
                    });
                 })
            }).blur(function() {
                //inputSearchList.hide();
                //inputS.parent().next().css("opacity", "1");
            });

            // удалить один елемент
            function initRemoveItem(element){
                $(element).on("click", function(){
                    var _this = $(this);
                    App.confirm(
                    "Вы действительно хотите удалить?", function(t){
                        if(t)
                        {
                           _this.parent().parent().parent().remove();
                        }
                    });

                });
            };

            // скрыть список
            function initHideItems(element){
                element.hide();
                element.parent().next().css("opacity", "1");
            };

            initRemoveItem(".products-list .fa-times");

            // скрыть список по клику за пределоми
            $(document).on('click', function (e) {
                if ($(e.target).closest(inputSearchList).length === 0 && $(e.target).closest(inputS).length === 0) {
                    initHideItems(inputSearchList);
                }
            });
        }
    };

    var handleAddDinamicField = function(){
          // Add dynamic fields through jquery
          var add_button = $(".add-new-line-btn");

           $(add_button).each(function(){
                var _line = $(this).attr("data-line");
                    $(_line).find(".delete-line").css("opacity", "0");

                  $(this).click(function(e){ //on add input button click
                      e.preventDefault();
                        var nLine = $(_line).children().first().clone();
                        $(_line).append(nLine);

                        nLine.find(".delete-line").css("opacity", "1").on("click", function(e){
                          e.preventDefault();
                          $(this).parent().parent().remove();
                      });
                }); // Here you need to update your code
           });
    };

    //cropper.js
    var handleCropper = function(){
        // на странице
        if($("#image-crop").length > 0)
        {
            var image = document.getElementById('image-crop');
            var cropper = new Cropper(image, {
              aspectRatio: 1 / 1,
              crop: function(e) {
                console.log(e.detail.x);
                console.log(e.detail.y);
              },
              movable: false,
              cropBoxResizable:true,
              //autoCrop: true,
              scalable: false,
              zoomable:false,
              preview:'.img-preview',
            });
        }

        // в модальном окне
        $(".cropper-modal-link").each(function(){

            var imgSrcCropp = $(this).attr("data-img-src");

            if($(this).attr("data-title"))
            {
                 var titleInModal = $(this).attr("data-title");
            }
            else{
                 var titleInModal = "Редактировать";
            }


           var htmlInModal = '<div class="row" id="cropp-body-in">'+'\
                            <div class="col-md-8">'+'\
                                <div class="cropp-img-wrapper">'+'\
                                    <img id="image-crop-mod" src="'+imgSrcCropp+'">'+'\
                                </div>'+'\
                            </div>'+'\
                            <div class="col-md-4">'+'\
                                <div class="docs-preview clearfix margin-top cropp-prev-wrapper">'+'\
                                    <div class="img-preview preview-lg margin-bottom"></div>'+'\
                                    <div class="clearfix">'+'\
                                        <div class="img-preview preview-md pull-left margin-bottom"></div>'+'\
                                        <div class="img-preview preview-sm pull-left margin-left margin-bottom"></div>'+'\
                                    </div>'+'\
                                    <div class="clearfix">'+'\
                                        <a class="btn btn-warning btnNext" href="#">Отклонить</a>'+'\
                                        <a class="btn btn-primary btnNext pull-right" href="#">Утвердить</a>'+'\
                                    </div>'+'\
                                </div>'+'\
                            </div>'+'\
                        </div>';

            $(this).on("click", function() {
              $("body").addClass("cropp-dialog-init");
              BootstrapDialog.show({
                   title: titleInModal,
                   message: htmlInModal,
                   closable: true,
                   draggable: false,
                   buttons: false,
                   onshown:function(){
                            var image = document.getElementById('image-crop-mod');
                            var cropper = new Cropper(image, {
                              aspectRatio: 1 / 1,
                              crop: function(e) {
                                console.log(e.detail.x);
                                console.log(e.detail.y);
                              },
                              movable: false,
                              cropBoxResizable:true,
                              //autoCrop: true,
                              scalable: false,
                              zoomable:false,
                              preview:'.img-preview',
                            });
                    }
                });

              return false;
            });
        });
    };

    // модалка с динамическим содержимым
    var handleWithInsetDataModals = function(){

        $(".show-modal-link").each(function(){

            var imgSrcCropp = $(this).attr("data-img-src");

            if($(this).attr("data-title"))
            {
                 var _titleInModal = $(this).attr("data-title");
            }
            else{
                 var _titleInModal = "Редактировать";
            }


            if($(this).attr("data-html"))
            {
                 var _htmlInModal = $(this).attr("data-html");
            }
            else{
                 var _htmlInModal = '<div class="row" id="cropp-body-in">'+'\
                                <div class="col-md-12">'+'\
                                    <img src="'+imgSrcCropp+'" width="300" height="300">'+'\
                                </div>'+'\
                            </div>';
            }

            $(this).on("click", function() {
              $("body").removeClass("cropp-dialog-init");
              if($(this).attr("data-footer") == "false"){
                $("body").addClass("remove-modal-footer");
              }
              else{
                $("body").removeClass("remove-modal-footer");
              }
              BootstrapDialog.show({
                   title: _titleInModal,
                   message: _htmlInModal,
                   closable: true,
                   draggable: false,
                   buttons: false,
                   buttons: [{
                                label: 'Отклонить',
                                cssClass: 'btn-warning',
                                action: function(){

                                }
                            },{
                                label: 'Подтвердить',
                                cssClass: 'btn-primary',
                                action: function(dialogItself){
                                    dialogItself.close();
                                }
                            }]
                });

              return false;
            });
        });
    };

 // модалка для модерации профайла
    var handleModerationModals = function(){

        $(".show-moderaton-modal").each(function(){

            if($(this).attr("data-title"))
            {
                 var _titleInModal = $(this).attr("data-title");
            }
            else{
                 var _titleInModal = "Редактировать";
            }


            if($(this).attr("data-html"))
            {
                 var _htmlInModal = $(this).attr("data-html");
            }
            else{
                 var _htmlInModal = '<div class="row" id="cropp-body-in">'+'\
                                <div class="col-md-12">'+'\
                                <textarea name="" class="form-control" rows="10"></textarea>'+'\
                                </div>'+'\
                            </div>';
            }

            $(this).on("click", function() {
              $("body").removeClass("cropp-dialog-init");
              if($(this).attr("data-footer") == "false"){
                $("body").addClass("remove-modal-footer");
              }
              else{
                $("body").removeClass("remove-modal-footer");
              }
              BootstrapDialog.show({
                   title: _titleInModal,
                   message: _htmlInModal,
                   closable: true,
                   draggable: false,
                   buttons: false,
                   buttons: [{
                                label: 'Отправить',
                                cssClass: 'btn-primary',
                                action: function(dialogItself){
                                    dialogItself.close();
                                }
                            }]
                });

              return false;
            });
        });
    };

    return {
        init: function() {
            // Placeholder для IE 9
            handleFixInputPlaceholderForIE();

            //UI компаненты
            //handleiCheck();
            handleSelect2();
            //handleDatepicker();
            handlePhotoViewer();
            handleScrollToTop();
            //handleScroll();
            handleModals();
            handleInputMask()
            handleWithInsetDataModals();

            //admin компаненты
            handleAutoShowInfoInModal();
            //handleFilterDatepicker();
            handleSubTable();
            handleHidePlaceholder();
            handleFileUploader();
            handleTabActive();
            handleMenuItemActive();
            handleAjaxSelect();
            handleAddDinamicField();
            handleModerationModals();
        },

        // wrApper function to scroll(focus) to an element
        scrollTo: function(el, offeset) {
            var pos = (el && el.length > 0) ? el.offset().top : 0;

            if (el) {
                if ($('body').hasClass('page-header-fixed')) {
                    pos = pos - $('.page-header').height();
                }
                pos = pos + (offeset ? offeset : -1 * el.height());
            }

            $('html,body').animate({
                scrollTop: pos
            }, 'fast');
        },

        // function to scroll to the top
        scrollTop: function() {
            App.scrollTo();
        },

        //public helper function to get actual input value(used in IE9 and IE8 due to placeholder attribute not supported)
        getActualVal: function(el) {
            el = $(el);
            if (el.val() === el.attr("placeholder")) {
                return "";
            }
            return el.val();
        },

        //public function to get a paremeter by name from URL
        getURLParameter: function(paramName) {
            var searchString = window.location.search.substring(1),
                i, val, params = searchString.split("&");

            for (i = 0; i < params.length; i++) {
                val = params[i].split("=");
                if (val[0] == paramName) {
                    return unescape(val[1]);
                }
            }
            return null;
        },

        // check for device touch support
        isTouchDevice: function() {
            try {
                document.createEvent("TouchEvent");
                return true;
            } catch (e) {
                return false;
            }
        },

        // To get the correct viewport width based on  http://andylangton.co.uk/articles/javascript/get-viewport-size-javascript/
        getViewPort: function() {
            var e = window,
                a = 'inner';
            if (!('innerWidth' in window)) {
                a = 'client';
                e = document.documentElement || document.body;
            }

            return {
                width: e[a + 'Width'],
                height: e[a + 'Height']
            };
        },

        getUniqueID: function(prefix) {
            return 'prefix_' + Math.floor(Math.random() * (new Date()).getTime());
        },

        getRandom: function(min, max) {
          return Math.floor(Math.random() * (max - min)) + min;
        },

        confirm: function(message, callback){
                swal({
                    title: message,
                    text: false,
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonText: "Да",
                    cancelButtonText: "Нет"
                },
                function(isConfirm){
                    callback(isConfirm);
            });
        },

        alert: function(txt){
             swal({
                    title: txt,
                    text: false,
                    type: "success",
                    showCancelButton: false,
                    confirmButtonText: "Ок"
                });
        },

        notify: function(text, type) {
            toastr.options.positionClass = "toast-top-right";
            toastr.options.timeOut = "2000";
            toastr.options.closeButton = true;

            // успешно
            if(type == "success")
            {
                 toastr.success(text);
            }

            // Инфо
            if(type == "info")
            {
                 toastr.info(text);
            }

            // Предупреждение
            if(type == "warning")
            {
                 toastr.warning(text);
            }

            // Ошибка
            if(type == "error")
            {
                 toastr.error(text);
            }

        }
    };

}();

jQuery(document).ready(function() {
   App.init(); // init
});
