// postcss.config.js
export default ({ env }) => ({
	plugins: {
		'autoprefixer': {
			// Autoprefixer options
			grid: true,
			flexbox: true
		},
		'postcss-html': {
			// PostCSS HTML options
		},
		...(env === 'production' && {
			'cssnano': {
				preset: ['default', {
					// Custom optimizations
					discardComments: {
						removeAll: true
					},
					colormin: true,
					calc: true,
					normalizeWhitespace: true
				}]
			}
		})
	}
});
